package com.rgb72.adventureearth.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.rgb72.adventureearth.R;

import java.util.ArrayList;


/**
 * Created by nutthawut on 11/15/16.
 */

public class DialogUtils {

    public static final String KEY_INPUT_MULTILINE_TEXT = "input_text";

    public interface OnDialogClickedListener {
        int POSITIVE = 1;
        int NEUTRAL = 2;
        int NEGATIVE = 3;

        void onButtonClicked(int event, Bundle value);
    }

    public static AlertDialog showAlertDialog(Context context, String title, String message,
                                              String positive, String neutral, String negative,
                                              final OnDialogClickedListener listener) {
        return showAlertDialog(context, title, message, positive, neutral, negative, false, listener);
    }

    public static AlertDialog showAlertDialog(Context context, String title, String message,
                                              String positive, String neutral, String negative,
                                              boolean isCancelable, final OnDialogClickedListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (title != null && !title.equals("")) {
            builder.setTitle(title);
        }

        builder.setMessage(message);
        builder.setCancelable(isCancelable);

        if (positive != null && !positive.equals("")) {
            builder.setPositiveButton(positive,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            if (listener != null) {
                                listener.onButtonClicked(OnDialogClickedListener.POSITIVE, new Bundle());
                            }
                        }
                    });
        }
        if (neutral != null && !neutral.equals("")) {
            builder.setNeutralButton(neutral,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            if (listener != null) {
                                listener.onButtonClicked(OnDialogClickedListener.NEUTRAL, new Bundle());
                            }
                        }
                    });
        }

        if (negative != null && !negative.equals("")) {
            builder.setNegativeButton(negative,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            if (listener != null) {
                                listener.onButtonClicked(OnDialogClickedListener.NEGATIVE, new Bundle());
                            }
                        }
                    });
        }

        AlertDialog alert = builder.create();

        try {
            if (context != null) {
                alert.show();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return alert;
    }

    public static void showMessageOK(Activity activity, String message) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setNegativeButton("OK", null)
                .create()
                .show();
    }

    public static void showMessageOKCancel(Activity activity, String message, DialogInterface.OnClickListener okListener,
                                           DialogInterface.OnClickListener cancelListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .create()
                .show();
    }

    public static void showMessageCancel(Activity activity, String message,
                                         DialogInterface.OnClickListener cancelListener) {

        if (cancelListener == null) {
            cancelListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            };
        }

        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setNegativeButton("OK", cancelListener)
                .create()
                .show();

    }

    public static void showListData(final Context context, String title, ArrayList<String> arrayList,
                                    final String key, int itemSelect, final OnDialogClickedListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);

        final CharSequence[] charSequences = arrayList.toArray(new CharSequence[arrayList.size()]);
        builder.setSingleChoiceItems(charSequences, itemSelect, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Bundle bundle = new Bundle();
                bundle.putString(key, charSequences[which].toString());
                listener.onButtonClicked(OnDialogClickedListener.POSITIVE, bundle);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(
                R.string.label_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }


}