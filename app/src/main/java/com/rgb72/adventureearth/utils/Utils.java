package com.rgb72.adventureearth.utils;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.DisplayMetrics;

import com.rgb72.adventureearth.model.ShopItem;
import com.rgb72.adventureearth.model.realm.StickerCollection;

import java.util.ArrayList;

/**
 * Created by nutthawut on 11/27/16.
 */
public class Utils {

    public static int convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int)px;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static String deviceId(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static boolean alreadyDownloaded(ArrayList<StickerCollection> mStickerCollections , ShopItem item){
        for (StickerCollection collection : mStickerCollections) {
            if( collection.getStickerInAppId().equalsIgnoreCase(item.getAndroid_inapp_id()) &&
                    collection.getStickerId().equalsIgnoreCase(item.getItemID())) {
                return true;
            }
        }
        return false;
    }
}
