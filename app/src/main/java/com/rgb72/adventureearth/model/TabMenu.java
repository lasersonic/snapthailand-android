package com.rgb72.adventureearth.model;

/**
 * Created by nutthawut on 11/9/16.
 */
public enum TabMenu {
    FILTER, EFFECT, TEXT, STICKER, FRAME, LOCATION
}
