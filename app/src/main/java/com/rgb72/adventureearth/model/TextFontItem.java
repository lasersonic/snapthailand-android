package com.rgb72.adventureearth.model;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by nutthawut on 9/17/16.
 */
public class TextFontItem {

    private String fontName;
    private float textSize;

    public TextFontItem(String fontName, int textSize) {
        this.fontName = fontName;
        this.textSize = textSize;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public float getTextSize() {
        return textSize;
    }

    public Typeface getTypeface(Context context) {
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
        return face;
    }

    public String getTypefaceName() {
        return "fonts/" + fontName;
    }

}
