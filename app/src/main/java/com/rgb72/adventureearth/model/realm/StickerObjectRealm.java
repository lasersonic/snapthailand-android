package com.rgb72.adventureearth.model.realm;

import io.realm.RealmObject;

/**
 * Created by nutthawut on 11/10/16.
 */
public class StickerObjectRealm extends RealmObject {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public StickerObject toModel() {
        StickerObject obj = new StickerObject();
        obj.setPath(getPath());
        return obj;
    }
}
