package com.rgb72.adventureearth.model;

/**
 * Created by nutthawut on 12/19/16.
 */
public class EffectItem extends CollectionItem {

    private int thumbnailImageActiveDrawable;

    public EffectItem(String itemName, int thumbnailImageDrawable, int thumbnailImageActiveDrawable) {
        super(itemName, thumbnailImageDrawable);
        this.thumbnailImageActiveDrawable = thumbnailImageActiveDrawable;
    }

    public int getThumbnailImageActiveDrawable() {
        return thumbnailImageActiveDrawable;
    }

}
