package com.rgb72.adventureearth.model.filter;

import android.os.Parcel;
import android.os.Parcelable;

import com.rgb72.adventureearth.model.FilterItem;
import com.rgb72.adventureearth.model.SubMenu;

/**
 * Created by nutthawut on 12/18/16.
 */
public class FilterParam implements Parcelable {

    public static final int BRIGHTNESS_DEFAULT = 0;
    public static final int CONTRAST_DEFAULT = 1;
    public static final int SATURATION_DEFAULT = 1;
    public static final int SHARPEN_DEFAULT = 0;

    private int filterImageName;
    private float intensity = 0.5f;
    private float brightness = BRIGHTNESS_DEFAULT;
    private float sharpen = SHARPEN_DEFAULT;
    private float contrast = CONTRAST_DEFAULT;
    private float saturation = SATURATION_DEFAULT;
    private float fade = 1;
    private int isCrop;
    private int isRotate;

    public FilterParam() {}

    protected FilterParam(Parcel in) {
        filterImageName = in.readInt();
        intensity = in.readFloat();
        brightness = in.readFloat();
        sharpen = in.readFloat();
        contrast = in.readFloat();
        saturation = in.readFloat();
        fade = in.readFloat();
        isCrop = in.readInt();
        isRotate = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(filterImageName);
        dest.writeFloat(intensity);
        dest.writeFloat(brightness);
        dest.writeFloat(sharpen);
        dest.writeFloat(contrast);
        dest.writeFloat(saturation);
        dest.writeFloat(fade);
        dest.writeInt(isCrop);
        dest.writeInt(isRotate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FilterParam> CREATOR = new Creator<FilterParam>() {
        @Override
        public FilterParam createFromParcel(Parcel in) {
            return new FilterParam(in);
        }

        @Override
        public FilterParam[] newArray(int size) {
            return new FilterParam[size];
        }
    };

    public int getFilterImageName() {
        return filterImageName;
    }

    public void setFilterImageName(int filterImageName) {
        this.filterImageName = filterImageName;
    }

    public float getIntensity() {
        return intensity;
    }

    public void setIntensity(float intensity) {
        this.intensity = intensity;
    }

    public float getBrightness() {
        return brightness;
    }

    public void setBrightness(float brightness) {
        this.brightness = brightness;
    }

    public float getSharpen() {
        return sharpen;
    }

    public void setSharpen(float sharpen) {
        this.sharpen = sharpen;
    }

    public float getContrast() {
        return contrast;
    }

    public void setContrast(float contrast) {
        this.contrast = contrast;
    }

    public float getSaturation() {
        return saturation;
    }

    public void setSaturation(float saturation) {
        this.saturation = saturation;
    }

    public float getFade() {
        return fade;
    }

    public void setFade(float fade) {
        this.fade = fade;
    }

    public void setIsCrop(Boolean isCrop) {
        this.isCrop = isCrop ? 1 : 0;
    }

    public void setIsRotate(Boolean isRotate) {
        this.isRotate = isRotate ? 1 : 0;
    }

    public Boolean isCrop() {
        return isCrop == 1;
    }

    public Boolean isRotate() {
        return isRotate == 1;
    }

    public double getFilterValueBySubMenu(SubMenu subMenu) {
        switch (subMenu) {
            case CONTRAST:
                return getContrast();
            case SATURATION:
                return getSaturation();
            case BRIGHTNESS:
                return getBrightness();
            case SHARPEN:
                return getSharpen();
            case FADE:
                return getFade();
            case INTENSITY:
                return getIntensity();
        }
        return 0;
    }

    public double getProgressValueBySubMenu(SubMenu subMenu) {

        if (subMenu == SubMenu.INTENSITY) {
            return getIntensity()*100f;
        }
        if (subMenu == SubMenu.BRIGHTNESS) {
            return getBrightness()* 50f * 5f;
        }
        if (subMenu == SubMenu.CONTRAST) {
            if (getContrast() < 1) {
                return (1 * getContrast()) * -50f ;
            }
            else {
                return ((getContrast() - 1) / 3) * 50f ;
            }
        }
        if (subMenu == SubMenu.SATURATION) {
            return (getSaturation()-1)*50f;
        }
        if (subMenu == SubMenu.SHARPEN) {
            return getSharpen()*12.5f;
        }
        if (subMenu == SubMenu.FADE){
            return getFade()*100f;
        }

        return 0;
    }


    public void setFilterValueBySubMenu(SubMenu subMenu, double progressValue) {
        if (subMenu == SubMenu.INTENSITY) {
            float intensity = (float)progressValue / 100;
            setIntensity(intensity);
        }
        if (subMenu == SubMenu.BRIGHTNESS) {
            float brightness = (float)progressValue / 50f / 5f;
            setBrightness(brightness);
        }
        if (subMenu == SubMenu.CONTRAST) {
            float contrast;
            if (progressValue < 0) {
                contrast = 1 - Math.abs((float)progressValue/ 75f);
            }
            else {
                contrast = 1 + ((float)progressValue / 75f * 3);
            }
            setContrast(contrast);
        }
        if (subMenu == SubMenu.SATURATION) {
            float saturation = (float)progressValue / 50f + 1;
            setSaturation(saturation);
        }
        if (subMenu == SubMenu.SHARPEN) {
            float sharpen = (float)progressValue / 12.5f;
            setSharpen(sharpen);
        }
        if (subMenu == SubMenu.FADE){
            setFade(0.2f + (0.8f * ((float) progressValue / 100.0f)));
        }
    }

}
