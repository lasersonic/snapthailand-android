package com.rgb72.adventureearth.model;

/**
 * Created by nutthawut on 9/17/16.
 */
public class FilterItem extends CollectionItem {

    private int filterLookupImageName;

    public FilterItem(String itemName, int thumbnailImage, int filterLookupImageName) {
        super(itemName, thumbnailImage);
        this.filterLookupImageName = filterLookupImageName;
    }

    public int getFilterLookupImageName() {
        return filterLookupImageName;
    }

    public void setFilterLookupImageName(int filterLookupImageName) {
        this.filterLookupImageName = filterLookupImageName;
    }
}
