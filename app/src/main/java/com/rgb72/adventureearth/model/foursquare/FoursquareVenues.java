package com.rgb72.adventureearth.model.foursquare;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nutthawut on 12/4/16.
 */
public class FoursquareVenues implements Parcelable {

    private String name;

    public String getName() {
        return name;
    }

    private FoursquareVenuesLocation location;

    public FoursquareVenuesLocation getLocation() {
        return location;
    }

    protected FoursquareVenues(Parcel in) {
        name = in.readString();
        location = in.readParcelable(FoursquareVenuesLocation.class.getClassLoader());
    }

    public static final Creator<FoursquareVenues> CREATOR = new Creator<FoursquareVenues>() {
        @Override
        public FoursquareVenues createFromParcel(Parcel in) {
            return new FoursquareVenues(in);
        }

        @Override
        public FoursquareVenues[] newArray(int size) {
            return new FoursquareVenues[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeParcelable(location, flags);
    }
}
