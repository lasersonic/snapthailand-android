package com.rgb72.adventureearth.model.foursquare;

import java.util.List;

/**
 * Created by nutthawut on 12/4/16.
 */
public class FoursquareVenuesResponse {

    private List<FoursquareVenues> venues;

    public List<FoursquareVenues> getVenues() {
        return venues;
    }
}
