package com.rgb72.adventureearth.model.foursquare;

/**
 * Created by nutthawut on 12/4/16.
 */
public class FoursquareResponse {


    private FoursquareMeta meta;
    private FoursquareVenuesResponse response;

    public FoursquareMeta getMeta() {
        return meta;
    }

    public FoursquareVenuesResponse getResponse() {
        return response;
    }
}
