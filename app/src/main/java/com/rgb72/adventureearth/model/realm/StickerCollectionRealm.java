package com.rgb72.adventureearth.model.realm;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nutthawut on 11/10/16.
 */
public class StickerCollectionRealm extends RealmObject {
    @PrimaryKey
    private String stickerId;
    private String stickerName;
    private RealmList<StickerObjectRealm> stickerLists;
    private String stickerInAppId;
    private String stickerOrderSequence;
    private int stickerThumbnailIndex;

    public int getStickerThumbnailIndex() {
        return stickerThumbnailIndex;
    }

    public void setStickerThumbnailIndex(int stickerThumbnailIndex) {
        this.stickerThumbnailIndex = stickerThumbnailIndex;
    }

    public RealmList<StickerObjectRealm> getStickerLists() {
        return stickerLists;
    }

    public void setStickerLists(RealmList<StickerObjectRealm> stickerLists) {
        this.stickerLists = stickerLists;
    }

    public String getStickerName() {
        return stickerName;
    }

    public void setStickerName(String stickerName) {
        this.stickerName = stickerName;
    }

    public String getStickerId() {
        return stickerId;
    }

    public void setStickerId(String stickerId) {
        this.stickerId = stickerId;
    }

    public String getStickerInAppId() {
        return stickerInAppId;
    }

    public void setStickerInAppId(String stickerInAppId) {
        this.stickerInAppId = stickerInAppId;
    }

    public String getStickerOrderSequence() {
        return stickerOrderSequence;
    }

    public void setStickerOrderSequence(String stickerOrderSequence) {
        this.stickerOrderSequence = stickerOrderSequence;
    }

    public StickerCollection toModel() {
        StickerCollection collection = new StickerCollection();
        collection.setStickerId(getStickerId());
        collection.setStickerName(getStickerName());
        collection.setStickerInAppId(getStickerInAppId());
        collection.setStickerOrderSequence(getStickerOrderSequence());

        ArrayList<StickerObject> stickerObjects = new ArrayList<>();
        for(StickerObjectRealm obj : getStickerLists()) {
            stickerObjects.add(obj.toModel());
        }
        collection.setStickerLists(stickerObjects);
        return collection;
    }
}
