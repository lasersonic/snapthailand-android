package com.rgb72.adventureearth.model;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.Gravity;

import com.rgb72.adventureearth.R;

import java.io.Serializable;

/**
 * Created by nutthawut on 11/23/16.
 */
public class EditingText implements Parcelable{

    private String text;
    private int gravity;
    private int resTextColorId;
    private String typefaceName;
    private int newResTextColorId;
    private String newTypefaceName;
    private float opacity = 1;
    private float newOpacity;
    private boolean isUpdate;
    private boolean isKeepSize = false;

    public EditingText() {
        this.text = "DOUBLE TAP TO EDIT";
        this.gravity = Gravity.TOP | Gravity.LEFT ;
        this.resTextColorId = 0;
        this.isUpdate = false;
    }

    protected EditingText(Parcel in) {
        text = in.readString();
        gravity = in.readInt();
        resTextColorId = in.readInt();
        typefaceName = in.readString();
        newResTextColorId = in.readInt();
        newTypefaceName = in.readString();
        opacity = in.readFloat();
        newOpacity = in.readFloat();
        isUpdate = in.readByte() != 0;     //myBoolean == true if byte != 0
        isKeepSize = in.readByte() != 0;
    }

    public static final Creator<EditingText> CREATOR = new Creator<EditingText>() {
        @Override
        public EditingText createFromParcel(Parcel in) {
            return new EditingText(in);
        }

        @Override
        public EditingText[] newArray(int size) {
            return new EditingText[size];
        }
    };

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getGravity() {
        return gravity;
    }

    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    public int getResTextColorId() {
        return resTextColorId;
    }

    public void setResTextColorId(int resTextColorId) {
        this.resTextColorId = resTextColorId;
    }

    public String getTypefaceName() {
        return typefaceName;
    }

    public void setTypefaceName(String typefaceName) {
        this.typefaceName = typefaceName;
    }

    public String getNewTypefaceName() {
        return newTypefaceName;
    }

    public void setNewTypefaceName(String newTypefaceName) {
        this.newTypefaceName = newTypefaceName;
    }

    public int getNewResTextColorId() {
        return newResTextColorId;
    }

    public void setNewResTextColorId(int newResTextColorId) {
        this.newResTextColorId = newResTextColorId;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public float getNewOpacity() {
        return newOpacity;
    }

    public void setNewOpacity(float newOpacity) {
        this.newOpacity = newOpacity;
    }

    public float getOpacity() {
        return opacity;
    }

    public void setOpacity(float opacity) {
        this.opacity = opacity;
    }

    public float getOpacityFromProgressValue(double progressValue) {
        float opacity = 0.2f + ((0.8f * (float)progressValue) / 100f);
        return opacity;
    }

    public boolean isKeepSize() {
        return isKeepSize;
    }

    public void setKeepSize(boolean keepSize) {
        this.isKeepSize = keepSize;
    }

    public double getProgressValueFromOpacity() {
        return (opacity-0.2f) * 100 / 0.8;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(text);
        parcel.writeInt(gravity);
        parcel.writeInt(resTextColorId);
        parcel.writeString(typefaceName);
        parcel.writeInt(newResTextColorId);
        parcel.writeString(newTypefaceName);
        parcel.writeFloat(opacity);
        parcel.writeFloat(newOpacity);
        parcel.writeByte((byte) (isKeepSize ? 1 : 0));     //if myBoolean == true, byte == 1
        parcel.writeByte((byte) (isUpdate ? 1 : 0));     //if myBoolean == true, byte == 1
    }
}
