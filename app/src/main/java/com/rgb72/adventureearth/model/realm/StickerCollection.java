package com.rgb72.adventureearth.model.realm;

import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by nutthawut on 11/10/16.
 */
public class StickerCollection {

    private String stickerId;
    private String stickerName;
    private String stickerInAppId;
    private String stickerOrderSequence;
    private int stickerThumbnailIndex = 0;
    private ArrayList<StickerObject> stickerLists;

    public int getStickerThumbnailIndex() {
        return stickerThumbnailIndex;
    }

    public void setStickerThumbnailIndex(int stickerThumbnailIndex) {
        this.stickerThumbnailIndex = stickerThumbnailIndex;
    }

    public String getStickerId() {
        return stickerId;
    }

    public void setStickerId(String stickerId) {
        this.stickerId = stickerId;
    }

    public String getStickerName() {
        return stickerName;
    }

    public void setStickerName(String stickerName) {
        this.stickerName = stickerName;
    }

    public ArrayList<StickerObject> getStickerLists() {
        return stickerLists;
    }

    public void setStickerLists(ArrayList<StickerObject> stickerLists) {
        this.stickerLists = stickerLists;
    }


    public String getStickerInAppId() {
        return stickerInAppId;
    }

    public void setStickerInAppId(String stickerInAppId) {
        this.stickerInAppId = stickerInAppId;
    }

    public String getStickerOrderSequence() {
        return stickerOrderSequence;
    }

    public void setStickerOrderSequence(String stickerOrderSequence) {
        this.stickerOrderSequence = stickerOrderSequence;
    }

    public StickerCollectionRealm toRealm() {
        StickerCollectionRealm collection = new StickerCollectionRealm();
        collection.setStickerId(getStickerId());
        collection.setStickerName(getStickerName());
        collection.setStickerInAppId(getStickerInAppId());
        collection.setStickerOrderSequence(getStickerOrderSequence());

        RealmList<StickerObjectRealm> stickerObjects = new RealmList<>();
        for(StickerObject obj : getStickerLists()) {
            stickerObjects.add(obj.toRealm());
        }
        collection.setStickerLists(stickerObjects);
        return collection;
    }
}
