package com.rgb72.adventureearth.model.foursquare;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by nutthawut on 12/4/16.
 */
public class FoursquareVenuesLocation implements Parcelable {

    private float lat;
    private float lng;
    private int distance;
    private String state;
    private String country;

    protected FoursquareVenuesLocation(Parcel in) {
        lat = in.readFloat();
        lng = in.readFloat();
        distance = in.readInt();
        state = in.readString();
        country = in.readString();
    }

    public static final Creator<FoursquareVenuesLocation> CREATOR = new Creator<FoursquareVenuesLocation>() {
        @Override
        public FoursquareVenuesLocation createFromParcel(Parcel in) {
            return new FoursquareVenuesLocation(in);
        }

        @Override
        public FoursquareVenuesLocation[] newArray(int size) {
            return new FoursquareVenuesLocation[size];
        }
    };

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }

    public int getDistance() {
        return distance;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(lat);
        dest.writeFloat(lng);
        dest.writeInt(distance);
        dest.writeString(state);
        dest.writeString(country);
    }
}
