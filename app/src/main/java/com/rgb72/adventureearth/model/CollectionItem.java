package com.rgb72.adventureearth.model;

import java.io.Serializable;

/**
 * Created by nutthawut on 9/17/16.
 */
public class CollectionItem implements Serializable{

    private String itemName;
    private int thumbnailImageDrawable;

    public CollectionItem(String itemName, int thumbnailImageDrawable) {
        this.itemName = itemName;
        this.thumbnailImageDrawable = thumbnailImageDrawable;
    }

    public int getThumbnailImage() {
        return thumbnailImageDrawable;
    }

    public void setThumbnailImage(int thumbnailImage) {
        this.thumbnailImageDrawable = thumbnailImage;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


    public SubMenu getSubMenu() {
        if (getItemName().toLowerCase().equals("crop")){
            return SubMenu.CROP;
        }
        else if (getItemName().toLowerCase().equals("rotate")){
            return SubMenu.ROTATE;
        }
        else if (getItemName().toLowerCase().equals("brightness")){
            return SubMenu.BRIGHTNESS;
        }
        else if (getItemName().toLowerCase().equals("contrast")){
            return SubMenu.CONTRAST;
        }
        else if (getItemName().toLowerCase().equals("saturation")){
            return SubMenu.SATURATION;
        }
        else if (getItemName().toLowerCase().equals("sharpen")){
            return SubMenu.SHARPEN;
        }
        else if (getItemName().toLowerCase().equals("fade")){
            return SubMenu.FADE;
        }
        else if (getItemName().toLowerCase().equals("opacity")){
            return SubMenu.FONT_OPACITY;
        }
        else if (getItemName().toLowerCase().equals("font")){
            return SubMenu.FONT;
        }
        else if (getItemName().toLowerCase().equals("color")){
            return SubMenu.FONT_COLOR;
        }
        return null;
    }
}
