package com.rgb72.adventureearth.model.realm;

/**
 * Created by nutthawut on 11/10/16.
 */
public class StickerObject {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public StickerObjectRealm toRealm() {
        StickerObjectRealm obj = new StickerObjectRealm();
        obj.setPath(getPath());
        return obj;
    }
}
