package com.rgb72.adventureearth.model;

/**
 * Created by nutthawut on 11/9/16.
 */
public enum SubMenu {
    INTENSITY, CROP, ROTATE, BRIGHTNESS, CONTRAST, SATURATION, SHARPEN, FADE,
    FONT, FONT_COLOR, FONT_OPACITY,
    LOCATION
}