package com.rgb72.adventureearth.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nutthawut on 8/28/16.
 */

public class ShopItem  implements Serializable{
    @SerializedName("id")
    private String itemID;
    @SerializedName("name")
    private String itemName;
    @SerializedName("description")
    private String itemDescription;
    private String avatar;

    private String order_sequence;

    private Double price;
    private String price_text;
    private String android_inapp_id;
    private List<String> preview_thumbnails;
    private List<String> thumbnails;

    public String getItemID() {
        return itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public String getAvatar() {
        return avatar;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAndroid_inapp_id() {
        return android_inapp_id;
    }

    public List<String> getPreview_thumbnails() {
        return preview_thumbnails;
    }

    public List<String> getThumbnails() {
        return thumbnails;
    }

    public String getPrice_text() {
        return price_text;
    }

    public void setPrice_text(String price_text) {
        this.price_text = price_text;
    }

    public String getSequence_order() {
        return order_sequence;
    }

}
