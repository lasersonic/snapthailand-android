package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.model.realm.StickerObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class StickerCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_SHOPPING = 0;
    private final int TYPE_STICKER_CAT = 1;

    private final PublishSubject<StickerCollection> onClickSubject = PublishSubject.create();
    private ArrayList<StickerCollection> items;
    private Context context;

    public class CartViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView)
        ImageView imageView;

        CartViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class StickerCollectionViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView)
        ImageView imageView;

        @Bind(R.id.lblTitle)
        TextView lblTitle;

        StickerCollectionViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public StickerCategoryAdapter(Context pContext, ArrayList<StickerCollection>  pItems) {
        context = pContext;
        items = pItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        if (viewType == TYPE_SHOPPING) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_sticker_shopping_cart, parent, false);

            return new CartViewHolder(view);

        }
        else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_effect, parent, false);
            StickerCollectionViewHolder holder = new StickerCollectionViewHolder(view);

            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CartViewHolder) {
            CartViewHolder cartViewHolder = (CartViewHolder)holder;
            Glide.with(context).load(R.drawable.shopping_cart).into(cartViewHolder.imageView);
        }
        else {
            StickerCollectionViewHolder collectionViewHolder = (StickerCollectionViewHolder)holder;

            StickerCollection collection = items.get(position - 1);
            if (collection == null) { return; }

            ArrayList<StickerObject> stickerList = collection.getStickerLists();
            if (stickerList.size() > 0) {
                Glide.with(context).load(new File(stickerList.get(collection.getStickerThumbnailIndex()).getPath())).into(collectionViewHolder.imageView);
                collectionViewHolder.lblTitle.setText(collection.getStickerName());
            }
        }
        
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    onClickSubject.onNext(null);
                }
                else {
                    onClickSubject.onNext(items.get(position - 1));
                }
            }
        });
    }

    public Observable<StickerCollection>  getPositionClicks() {
        return onClickSubject.asObservable();
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_SHOPPING;
        }
        return TYPE_STICKER_CAT;
    }

    @Override
    public int getItemCount() {
        return items.size() + 1;
    }

}