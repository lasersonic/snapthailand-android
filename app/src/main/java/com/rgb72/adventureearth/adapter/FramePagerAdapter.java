package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.foursquare.FoursquareVenues;
import com.rgb72.adventureearth.views.NTBackgroundTextView;
import com.rgb72.adventureearth.views.NTTextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;

/**
 * Created by nutthawut on 12/3/16.
 */
public class FramePagerAdapter extends PagerAdapter {


    private LayoutInflater mInflater;
    private Context mContext;



    private FoursquareVenues mVenues;
    private Date mDate;

    public FramePagerAdapter(FoursquareVenues venues, Context context) {
        mVenues = venues;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mDate = Calendar.getInstance().getTime();
    }

    public void setVenue(FoursquareVenues mVenues) {
        this.mVenues = mVenues;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return 8;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = mInflater.inflate(getResourceIdFromAdapter(position), container, false);

        view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        setItemLocationView(view, position);

        view.setTag(position);

        container.addView(view, 0);

        return view;
    }

    public static int getResourceIdFromAdapter(int position) {
        int resourceId = R.layout.item_location_1;

        switch (position) {
            case 0:
                resourceId = R.layout.item_location_1;
                break;
            case 1:
                resourceId = R.layout.item_location_2;
                break;
            case 2:
                resourceId = R.layout.item_location_3;
                break;
            case 3:
                resourceId = R.layout.item_location_4;
                break;
            case 4:
                resourceId = R.layout.item_location_5;
                break;
            case 5:
                resourceId = R.layout.item_location_6;
                break;
            case 6:
                resourceId = R.layout.item_location_7;
                break;
            case 7:
                resourceId = R.layout.item_location_8;
                break;
        }

        return resourceId;
    }


    private String getDateTimeFormat(int position) {

        switch (position) {
            case 0:
                return "MMM dd,yyyy";
            case 1:
                return "HH:mm a / MMM dd,yyyy";
            case 3:
                return "MMM dd,yyyy";
            case 7:
                return "MMM dd,yyyy";
            default:
                return null;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public int getItemPosition(Object object) {

        int position = super.getItemPosition(object);

        if (object instanceof View) {
            View view = (View)object;
            setItemLocationView(view, position);
        }

        return position;
    }

    void setItemLocationView(View view, int position) {
        TextView locationTextView = (TextView)view.findViewById(R.id.location_textView);
        TextView locationTextView2 = (TextView)view.findViewById(R.id.location2_textView);
        TextView timeTextView = (TextView)view.findViewById(R.id.time_textView);

        if (locationTextView != null) {
            if (locationTextView instanceof NTBackgroundTextView) {
                ((NTBackgroundTextView) locationTextView).setBackgroundOnlyText(mVenues.getName());
            }
            else {
                locationTextView.setText(mVenues.getName());
            }
        }
        if (locationTextView2 != null) {
            locationTextView2.setText(mVenues.getLocation().getState() + " | " + mVenues.getLocation().getCountry());
        }
        if (getDateTimeFormat(position) != null && timeTextView != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(getDateTimeFormat(position), Locale.US);
            timeTextView.setText(dateFormat.format(mDate));
        }
    }
}
