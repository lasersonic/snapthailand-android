package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.manager.RealmHelper;
import com.rgb72.adventureearth.model.ShopItem;
import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ShopCellHolder> {

    private final PublishSubject<ShopItem> onClickSubject = PublishSubject.create();
    private final PublishSubject<ShopItem> onClickButtonBuy = PublishSubject.create();
    private ArrayList<ShopItem> shopItemList;
    private ArrayList<StickerCollection> mStickerCollections;

    public class ShopCellHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;
        @Bind(R.id.lblSubTitle)
        TextView lblSubtitle;
        @Bind(R.id.mainThumb)
        ImageView mainThumb;
        @Bind(R.id.imageThumbnail1)
        ImageView imageThumbnail1;
        @Bind(R.id.imageThumbnail2)
        ImageView imageThumbnail2;
        @Bind(R.id.btnBuy)
        Button btnBuy;

        ShopCellHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private Context context;

    public ShopAdapter(Context context, ArrayList<ShopItem> shopItemList, ArrayList<StickerCollection> downloadedList) {
        this.shopItemList = shopItemList;
        this.context = context;
        this.mStickerCollections = downloadedList;
    }


    @Override
    public ShopCellHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_shop, parent, false);
        ShopCellHolder holder = new ShopCellHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ShopCellHolder holder, final int position) {

        final ShopItem item = this.shopItemList.get(position);

        holder.lblTitle.setText(item.getItemName());
        holder.lblSubtitle.setText(item.getThumbnails().size() + " Images");
        Uri avatar = Uri.parse(item.getAvatar());

        Glide.with(context).load(avatar).into(holder.mainThumb);

        for (int i = 0; i < Math.min(2, item.getPreview_thumbnails().size()); i++) {

            ImageView img = null;
            switch (i) {
                case 0:
                    img = holder.imageThumbnail1;
                    break;
                case 1:
                    img = holder.imageThumbnail2;
                    break;
                default:
                    break;
            }

            Uri uri = Uri.parse(item.getPreview_thumbnails().get(i));

            if (img != null) {
                Glide.with(context).load(uri).into(img);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(item);
            }
        });

        if (Utils.alreadyDownloaded(mStickerCollections, item)) {
            holder.btnBuy.setVisibility(View.INVISIBLE);
            holder.btnBuy.setOnClickListener(null);
        } else {
            holder.btnBuy.setVisibility(View.VISIBLE);
            holder.btnBuy.setText(item.getPrice_text());
            holder.btnBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickButtonBuy.onNext(item);
                }
            });
        }

    }


    public Observable<ShopItem> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    public Observable<ShopItem> getShopItemBuyObservable() {
        return onClickButtonBuy.asObservable();
    }

    @Override
    public int getItemCount() {
        return shopItemList.size();
    }

}