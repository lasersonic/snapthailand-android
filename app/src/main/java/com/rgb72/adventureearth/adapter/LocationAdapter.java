package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.ShopItem;
import com.rgb72.adventureearth.model.foursquare.FoursquareVenues;
import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 5/13/17.
 */


public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.TextCellHolder> {

    private final PublishSubject<FoursquareVenues> onClickSubject = PublishSubject.create();
    private List<FoursquareVenues> placeList;

    public class TextCellHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;

        @Bind(R.id.lblSubTitle)
        TextView lblSubtitle;

        TextCellHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private Context context;

    public LocationAdapter(Context context, List<FoursquareVenues> placeList) {
        this.placeList = placeList;
        this.context = context;
    }


    @Override
    public TextCellHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_text_subtitle, parent, false);
        TextCellHolder holder = new TextCellHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(TextCellHolder holder, final int position) {
        final FoursquareVenues item = this.placeList.get(position);
        holder.lblTitle.setText(item.getName());
        holder.lblSubtitle.setText(item.getLocation().getState() + ", " + item.getLocation().getCountry());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(item);
            }
        });
    }

    public Observable<FoursquareVenues> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return placeList.size();
    }

}