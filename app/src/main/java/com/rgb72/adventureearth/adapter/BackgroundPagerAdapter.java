package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.foursquare.FoursquareVenues;
import com.rgb72.adventureearth.views.NTBackgroundTextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nutthawut on 12/3/16.
 */
public class BackgroundPagerAdapter extends PagerAdapter {


    private LayoutInflater mInflater;
    private Context mContext;

    public BackgroundPagerAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = mInflater.inflate(R.layout.pager_image, container, false);
        ImageView imageView = (ImageView)view.findViewById(R.id.imageView);
        Glide.with(mContext).load(getImageResourcesId(position)).into(imageView);
        container.addView(view, 0);
        return view;
    }

    private int getImageResourcesId(int position) {
        switch (position) {
            case 0:
                return R.drawable.bg_1;
            case 1:
                return R.drawable.bg_2;
            case 2:
                return R.drawable.bg_3;
            case 3:
                return R.drawable.bg_4;
            default:
                return R.drawable.bg_5;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
