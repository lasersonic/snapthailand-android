package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.manager.RealmHelper;
import com.rgb72.adventureearth.model.ShopItem;
import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class ShopDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_HEADER = 0, TYPE_ROW = 1, COLUMN_NUM = 4;
    private final PublishSubject<Void> onClickSubject = PublishSubject.create();
    private ShopItem shopItem;
    private Context context;

    private ArrayList<StickerCollection> mStickerCollections;

    public class ShopCellHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageThumbnail1)
        ImageView imageThumbnail1;
        @Bind(R.id.imageThumbnail2)
        ImageView imageThumbnail2;
        @Bind(R.id.imageThumbnail3)
        ImageView imageThumbnail3;
        @Bind(R.id.imageThumbnail4)
        ImageView imageThumbnail4;

        ShopCellHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class ShopDetailTopItemHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;
        @Bind(R.id.mainThumb)
        ImageView mainThumb;
        @Bind(R.id.lblDetail)
        TextView lblDetail;
        @Bind(R.id.btnBuy)
        Button btnBuy;

        ShopDetailTopItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ShopDetailAdapter(Context context, ShopItem shopItem) {
        this.shopItem = shopItem;
        this.context = context;
        mStickerCollections = RealmHelper.listStickerCollection(context);

    }

    public void setmStickerCollections(ArrayList<StickerCollection> mStickerCollections) {
        this.mStickerCollections = mStickerCollections;
    }


    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? TYPE_HEADER : TYPE_ROW;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_shop_detail_header, parent, false);
            ShopDetailTopItemHolder holder = new ShopDetailTopItemHolder(view);
            return holder;
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_shop_detail_items, parent, false);
            ShopCellHolder holder = new ShopCellHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ShopItem item = this.shopItem;

        if (holder instanceof ShopDetailTopItemHolder) {
            ShopDetailTopItemHolder topItemHolder = (ShopDetailTopItemHolder) holder;

            String url = item.getAvatar();
            Glide.with(context).load(Uri.parse(url)).into(topItemHolder.mainThumb);

            topItemHolder.lblTitle.setText(item.getItemName());
            topItemHolder.lblDetail.setText(item.getItemDescription());

            if (Utils.alreadyDownloaded(mStickerCollections, item)) {
                topItemHolder.btnBuy.setText(context.getString(R.string.buttonDownloaded));
                topItemHolder.btnBuy.setOnClickListener(null);
                topItemHolder.btnBuy.setBackgroundColor(ContextCompat.getColor(context, R.color.buyButtonDownloadedBackground));
            } else {
                topItemHolder.btnBuy.setText(item.getPrice_text());
                topItemHolder.btnBuy.setBackgroundColor(ContextCompat.getColor(context, R.color.buyButtonBackground));
                topItemHolder.btnBuy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickSubject.onNext(null);
                    }
                });
            }
            return;
        }

        ShopCellHolder itemHolder = (ShopCellHolder) holder;

        int startIndex = (position - 1) * COLUMN_NUM;
        for (int i = startIndex; i < startIndex + COLUMN_NUM; i++) {

            ImageView imageView = null;
            switch (i % COLUMN_NUM) {
                case 0:
                    imageView = itemHolder.imageThumbnail1;
                    break;
                case 1:
                    imageView = itemHolder.imageThumbnail2;
                    break;
                case 2:
                    imageView = itemHolder.imageThumbnail3;
                    break;
                default:
                    imageView = itemHolder.imageThumbnail4;
                    break;
            }

            if (i < item.getThumbnails().size()) {

                if (imageView != null) {
                    String url = item.getThumbnails().get(i);
                    Glide.with(context).load(Uri.parse(url)).into(imageView);
                    imageView.setVisibility(View.VISIBLE);
                }

            } else {
                imageView.setVisibility(View.INVISIBLE);
            }
        }

    }

    public Observable<Void> getBuyClick() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return getItemRowCount() + 1;
    }

    private int getItemRowCount() {
        return (int) (Math.ceil(this.shopItem.getThumbnails().size() / (float)COLUMN_NUM));
    }

}