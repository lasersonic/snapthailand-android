package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.views.CircleView;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class ViewColorAdapter extends RecyclerView.Adapter<ViewColorAdapter.MyViewHolder> {

    private final PublishSubject<Integer> onClickSubject = PublishSubject.create();
    private int[] items;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.viewCircle)
        CircleView viewCircle;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ViewColorAdapter(Context context, int[] items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_color, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.viewCircle.setCircleColor(items[position]);

        if (position == 0) {
            holder.viewCircle.setBorder(Color.WHITE);
        }
        else {
            holder.viewCircle.setBorder(0);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(items[position]);
            }
        });

    }

    public Observable<Integer> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

}