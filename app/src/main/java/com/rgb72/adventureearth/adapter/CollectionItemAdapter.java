package com.rgb72.adventureearth.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.custom.ItemSize;
import com.rgb72.adventureearth.model.CollectionItem;
import com.rgb72.adventureearth.model.filter.FilterParam;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class CollectionItemAdapter extends RecyclerView.Adapter<CollectionItemAdapter.MyViewHolder> {

    private final PublishSubject<CollectionItem> onClickSubject = PublishSubject.create();
    private List<CollectionItem> items;
    private Context context;
    private ItemSize size;
    private boolean[] enableLists;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;

        @Bind(R.id.imageView)
        ImageView imageView;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public CollectionItemAdapter(Context context, List<CollectionItem> items) {
        this.context = context;
        this.items = items;
        this.size = ItemSize.WRAP;
        this.enableLists = new boolean[items.size()];
        Arrays.fill(enableLists, true);
    }

    public CollectionItemAdapter(Context context, List<CollectionItem> items, ItemSize size) {
        this.context = context;
        this.items = items;
        this.size = size;
        this.enableLists = new boolean[items.size()];
        Arrays.fill(enableLists, true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_effect, parent, false);
        MyViewHolder holder = new MyViewHolder(view);


        if(this.size == ItemSize.SCREEN_RATIO) {
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
            view.getLayoutParams().width = metrics.widthPixels / items.size();
        }


        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CollectionItem item = items.get(position);

        String text = item.getItemName();
        int drawable = item.getThumbnailImage();

        holder.lblTitle.setText(text);
        holder.imageView.setImageDrawable(ContextCompat.getDrawable(context, drawable));

        if (enableLists[position]){
            holder.lblTitle.setTextColor(ContextCompat.getColor(context, R.color.whiteColor));
        }
        else {
            if (position == 1) {
                holder.imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.color_inactive));
            }
            else if (position == 2) {
                holder.imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.fade_inactive));
            }
            holder.lblTitle.setTextColor(ContextCompat.getColor(context, R.color.whiteColorOpacity50));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!enableLists[position]) {
                    return;
                }
                onClickSubject.onNext(item);
            }
        });

    }

    public Observable<CollectionItem> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setEnableLists(boolean[] enableLists) {
        this.enableLists = enableLists;
    }
}