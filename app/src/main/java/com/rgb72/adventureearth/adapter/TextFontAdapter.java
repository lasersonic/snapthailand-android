package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.TextFontItem;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class TextFontAdapter extends RecyclerView.Adapter<TextFontAdapter.MyViewHolder> {

    private final PublishSubject<TextFontItem> onClickSubject = PublishSubject.create();
    private List<TextFontItem> items;
    private Context context;
    private int selectedPosition = -1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public TextFontAdapter(Context context, List<TextFontItem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_text, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final TextFontItem fontItem = items.get(position);
        float fontSize = fontItem.getTextSize();
        Typeface face = fontItem.getTypeface(context);

        holder.lblTitle.setText("ABC");
        holder.lblTitle.setTextSize(fontSize);
        holder.lblTitle.setTypeface(face);
        holder.lblTitle.setTextColor(ContextCompat.getColor(context, selectedPosition == position ? R.color.activeText : R.color.whiteColor));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int oldPosition = selectedPosition;
                selectedPosition = position;
                notifyItemChanged(selectedPosition);
                notifyItemChanged(oldPosition);
                onClickSubject.onNext(fontItem);
            }
        });

    }

    public Observable<TextFontItem> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}