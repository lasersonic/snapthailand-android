package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rgb72.adventureearth.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.MyViewHolder> {
    final int TYPE_HEADER = 1, TYPE_ROW = 2, TYPE_FOOTER = 0;

    private final PublishSubject<Integer> onClickSubject = PublishSubject.create();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }

    private Context context;

    public SettingAdapter(Context context) {
        this.context = context;
    }


    @Override
    public int getItemViewType(int position) {

        if (position == 0 || position == 3) {
            return TYPE_HEADER;
        }

        if (position == 6) {
            return TYPE_FOOTER ;
        }

        return TYPE_ROW;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ROW) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_setting, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        View view2 = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_setting_section, parent, false);

        MyViewHolder holder = new MyViewHolder(view2);

        if (viewType == TYPE_FOOTER) {
            holder.lblTitle.setTextColor(context.getResources().getColor(R.color.whiteColor));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)holder.lblTitle.getLayoutParams();
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            holder.lblTitle.setLayoutParams(layoutParams);
            holder.lblTitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
        else {
            holder.lblTitle.setGravity(Gravity.LEFT);
        }

        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String text = "";

        switch (position) {
            case 0: text = "PURCHASING";
                break;
            case 1: text = "Sticker shop";
                break;
            case 2: text = "Restore purchase";
                break;
            case 3: text = "OTHER";
                break;
            case 4 :text = "Send feedback";
                break;
            case 5: text = "Rate this app";
                break;
            case 6:
                try {
                    PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    text = "Adventure Earth version " + pInfo.versionName + "\n@2016, RGB72";

                }catch (PackageManager.NameNotFoundException nnfe) {
                    text = "Adventure Earth version 1.0\n@2016, RGB72";
                }

                break;
            default:break;
        }

        holder.lblTitle.setText(text);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(position);
            }
        });

    }

    public Observable<Integer> getPositionClicks(){
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return 7;
    }

}