package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.FilterItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder> {

    private final PublishSubject<FilterItem> onClickSubject = PublishSubject.create();
    private Context context;
    private ArrayList<FilterItem> items;
    private ArrayList<Bitmap> bitmapArrayList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;

        @Bind(R.id.imageView)
        ImageView imageView;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public FilterAdapter(Context context, ArrayList<FilterItem> items, ArrayList<Bitmap> bitmapArrayList) {
        this.context = context;
        this.items = items;
        this.bitmapArrayList = bitmapArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_filter, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (items == null || items.size() <= position) {
            return;
        }
        final FilterItem filterItem = items.get(position);
        Bitmap bitmap = this.bitmapArrayList.get(position);

        String text = filterItem.getItemName();

        holder.lblTitle.setText(text);

        holder.imageView.setImageBitmap(bitmap);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(filterItem);
            }
        });
    }

    public Observable<FilterItem> getPositionClicks(){
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return bitmapArrayList.size();
    }

}