package com.rgb72.adventureearth.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.BoolRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.custom.ItemSize;
import com.rgb72.adventureearth.model.EffectItem;
import com.rgb72.adventureearth.model.SubMenu;
import com.rgb72.adventureearth.model.filter.FilterParam;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class EffectItemAdapter extends RecyclerView.Adapter<EffectItemAdapter.MyViewHolder> {

    private final PublishSubject<EffectItem> onClickSubject = PublishSubject.create();
    private List<EffectItem> mItems;
    private Context mContext;
    private ItemSize mSize;
    private FilterParam mFilterParam;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.lblTitle)
        TextView lblTitle;

        @Bind(R.id.imageView)
        ImageView imageView;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public EffectItemAdapter(Context context, List<EffectItem> items, FilterParam filterParam) {
        this.mContext = context;
        this.mItems = items;
        this.mFilterParam = filterParam;
        this.mSize = ItemSize.WRAP;
    }

    public EffectItemAdapter(Context context, List<EffectItem> items, ItemSize size) {
        this.mContext = context;
        this.mItems = items;
        this.mSize = size;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_effect, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        if(this.mSize == ItemSize.SCREEN_RATIO) {
            DisplayMetrics metrics = new DisplayMetrics();
            ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
            view.getLayoutParams().width = metrics.widthPixels / mItems.size();
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final EffectItem item = mItems.get(position);

        int drawable;

        Boolean isActive = false;

        if (item.getSubMenu() == SubMenu.CROP && mFilterParam.isCrop()){
            isActive = true;
        }
        else if (item.getSubMenu() == SubMenu.ROTATE && mFilterParam.isRotate()){
            isActive = true;
        }
        else if (item.getSubMenu() == SubMenu.FADE && Math.floor(Math.abs(mFilterParam.getProgressValueBySubMenu(item.getSubMenu()))) < 100) {
            isActive = true;
        }
        else if (item.getSubMenu() != SubMenu.FADE && Math.floor(Math.abs(mFilterParam.getProgressValueBySubMenu(item.getSubMenu()))) != 0) {
            isActive = true;
        }

        if (isActive) {
            drawable = item.getThumbnailImageActiveDrawable();
            holder.lblTitle.setTextColor(ContextCompat.getColor(mContext, R.color.activeText));
        } else {
            drawable = item.getThumbnailImage();
            holder.lblTitle.setTextColor(ContextCompat.getColor(mContext, R.color.whiteColor));
        }
        holder.lblTitle.setText(item.getItemName());
        holder.imageView.setImageDrawable(ContextCompat.getDrawable(mContext, drawable));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(item);
            }
        });

    }

    public Observable<EffectItem> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

}