package com.rgb72.adventureearth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.realm.StickerObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 7/24/16.
 */


public class StickerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_BACK = 0;
    private final int TYPE_STICKER_CAT = 1;

    private final PublishSubject<StickerObject> onClickSubject = PublishSubject.create();
    private ArrayList<StickerObject> items;
    private Context context;

    public class ShoppingCartHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView)
        ImageView imageView;

        @Bind(R.id.textViewDownloadNow)
        TextView textView;

        ShoppingCartHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class StickerHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView)
        ImageView imageView;

        StickerHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public StickerAdapter(Context pContext, ArrayList<StickerObject> pItems) {
        context = pContext;
        items = pItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        if (viewType == TYPE_BACK) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_sticker_shopping_cart, parent, false);
            ShoppingCartHolder holder = new ShoppingCartHolder(view);
            return holder;
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_sticker, parent, false);
            StickerHolder holder = new StickerHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (getItemViewType(position) == TYPE_BACK) {
            ShoppingCartHolder shoppingCartHolder = (ShoppingCartHolder) holder;
            Glide.with(context).load(R.drawable.close_small).into(shoppingCartHolder.imageView);
            shoppingCartHolder.textView.setVisibility(View.GONE);
        } else {
            StickerHolder stickerHolder = (StickerHolder) holder;
            Glide.with(context).load(new File(items.get(position - 1).getPath())).into(stickerHolder.imageView);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getItemViewType(position) == TYPE_BACK) {
                    onClickSubject.onNext(null);
                } else {
                    onClickSubject.onNext(items.get(position - 1));
                }
            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_BACK;
        }
        return TYPE_STICKER_CAT;
    }

    public Observable<StickerObject> getPositionClicks() {
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return items.size() + 1;
    }

}