package com.rgb72.adventureearth.api;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import rx.Observable;

/**
 * Created by nutthawut on 8/28/16.
 */
public interface StickerDownloadService {

    @FormUrlEncoded
    @Streaming
    @POST("api/v1/sticker-collections/download")
    Observable<Response<ResponseBody>> download(@Field("data") String data);

}
