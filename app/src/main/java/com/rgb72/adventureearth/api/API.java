package com.rgb72.adventureearth.api;

import android.content.Context;
import android.util.Log;

import com.rgb72.adventureearth.manager.UnzipManager;
import com.rgb72.adventureearth.model.ShopItem;
import com.rgb72.adventureearth.utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by nutthawut on 8/28/16.
 */

public class API {

    private final String TAG = "nut";
    private static API instance;

    public static API getInstance(Context context) {
        if(instance==null) {
            instance = new API(context);
        }
        return instance;
    }

    private Context mContext;
    private Retrofit retrofit;
    private final String BASE_URL = "http://snapthailand.rgb72.net/";
    private String folderName;
    private UnzipManager.UnzipManagerCallback mUnzipCallback;

    public interface CallbackShop {
        void onSuccess(List<ShopItem> items);
        void onFail(String reason);
    }

    public interface CallbackRequest {
        void onSuccess(boolean success);
    }

    private API(Context context) {
         this.mContext = context;
         this.folderName = "/data/data/"+context.getPackageName()+"/stickers";
         this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build();

    }

    public void getStickerShop(final CallbackShop callback){

        StickerCollectionService service = retrofit.create(StickerCollectionService.class);

        Call<ShopItemResponse> call = service.request();
        call.enqueue(new Callback<ShopItemResponse>() {
            @Override
            public void onResponse(Call<ShopItemResponse> call, Response<ShopItemResponse> response) {

                List<ShopItem> items = response.body().getData();
                Log.d("nut", "items size = " + items.size());
                callback.onSuccess(items);
            }

            @Override
            public void onFailure(Call<ShopItemResponse> call, Throwable t) {
                callback.onFail(t.getLocalizedMessage());
            }
        });
    }

    public void sendPurchased(ShopItem item, final CallbackRequest pCallback) {

        SendPurchaseService service = retrofit.create(SendPurchaseService.class);
        
        String deviceId = Utils.deviceId(mContext);
        String data = "{\"sticker_collection_id\": "+item.getItemID()+",\"platform\": \"android\", \"device_id\": \""+ deviceId +"\"}";

        Call call = service.request(data);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (pCallback != null) {
                    pCallback.onSuccess(response.isSuccessful());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (pCallback != null) {
                    pCallback.onSuccess(false);
                }
            }
        });

    }

    public void sendLoginFacebookStat(JSONObject jsonObject, final CallbackRequest pCallback) {

        FacebookLoginService service = retrofit.create(FacebookLoginService.class);

        //Example of data
        //{"fbid":"1234", "email":"aaaa", "first_name":"bbbbb", "last_name":"cccc"}

        String data = null;

        try {
            data = "{\"fbid\": \"" + jsonObject.getString("id") + "\""
                    + ",\"email\": \"" + jsonObject.getString("email") + "\""
                    + ",\"first_name\": \"" + jsonObject.getString("first_name") + "\""
                    + ",\"last_name\": \"" + jsonObject.getString("last_name") + "\"}";

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (data == null) {
            return;
        }

        Call call = service.request(data);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (pCallback != null) {
                    pCallback.onSuccess(response.isSuccessful());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (pCallback != null) {
                    pCallback.onSuccess(false);
                }
            }
        });

    }



    public void downloadSticker(ShopItem item, DownloadProgressInterceptor.DownloadProgressListener downloadCallback, UnzipManager.UnzipManagerCallback unzipManagerCallback) {
        mUnzipCallback = unzipManagerCallback;
        downloadZipFileRx(item, downloadCallback);
    }

    private void downloadZipFileRx(ShopItem item, DownloadProgressInterceptor.DownloadProgressListener downloadCallback) {

        DownloadProgressInterceptor downloadProgressInterceptor = new DownloadProgressInterceptor(downloadCallback);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(downloadProgressInterceptor)
                .build();

        String deviceId = Utils.deviceId(mContext);
        String data = "{\"sticker_collection_id\": "+item.getItemID()+",\"platform\": \"android\", \"device_id\": \""+ deviceId +"\"}";
        StickerDownloadService service = createService(StickerDownloadService.class, okHttpClient, BASE_URL);
        service.download(data)
                .flatMap(processResponse(item))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(handleResult(item));

    }

    private Func1<Response<ResponseBody>, Observable<File>> processResponse(final ShopItem shopItem) {
        return new Func1<Response<ResponseBody>, Observable<File>>() {
            @Override
            public Observable<File> call(Response<ResponseBody> responseBodyResponse) {
                return saveToDiskRx(responseBodyResponse, shopItem);
            }
        };
    }

    private Observable<File> saveToDiskRx(final Response<ResponseBody> response, final ShopItem shopItem) {
        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(Subscriber<? super File> subscriber) {
                try {

                    new File(folderName).mkdir();
                    File destinationFile = new File(folderName + "/" + shopItem.getItemID() + ".zip");

                    BufferedSink bufferedSink = Okio.buffer(Okio.sink(destinationFile));
                    bufferedSink.writeAll(response.body().source());
                    bufferedSink.close();

                    subscriber.onNext(destinationFile);
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }
            }
        });
    }

    private Observer<File> handleResult(final ShopItem item) {
        return new Observer<File>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                Log.d(TAG, "Error " + e.getMessage());
            }

            @Override
            public void onNext(File file) {
                Log.d(TAG, "File downloaded to " + file.getAbsolutePath());
                UnzipManager unzipManager = new UnzipManager(mContext, item, mUnzipCallback);
                unzipManager.execute(file.getAbsolutePath(), folderName + "/unzip/" + item.getItemID());
            }
        };
    }

    public <T> T createService(Class<T> serviceClass,OkHttpClient client, String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build();
        return retrofit.create(serviceClass);
    }




}