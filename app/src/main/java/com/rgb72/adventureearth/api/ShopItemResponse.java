package com.rgb72.adventureearth.api;

import com.rgb72.adventureearth.model.ShopItem;

import java.util.List;

/**
 * Created by nutthawut on 8/28/16.
 */
public class ShopItemResponse {
    private List<ShopItem> data;

    public List<ShopItem> getData() {
        return data;
    }

    public void setData(List<ShopItem> data) {
        this.data = data;
    }
}

