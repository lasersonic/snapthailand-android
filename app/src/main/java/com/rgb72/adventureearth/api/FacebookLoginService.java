package com.rgb72.adventureearth.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by nutthawut on 5/13/17.
 */

public interface FacebookLoginService {

    @FormUrlEncoded
    @POST("api/v1/facebook/saveuser")
    Call<Response<ResponseBody>> request(@Field("data") String data);

}
