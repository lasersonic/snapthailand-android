package com.rgb72.adventureearth.api;

import com.rgb72.adventureearth.model.foursquare.FoursquareVenues;

import java.util.List;

/**
 * Created by nutthawut on 12/4/16.
 */
public interface FoursquareCallback {

    void onSuccess(List<FoursquareVenues> venuesList);
    void onError();

}
