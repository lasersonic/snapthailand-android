package com.rgb72.adventureearth.api;

import com.rgb72.adventureearth.model.foursquare.FoursquareResponse;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by nutthawut on 12/4/16.
 */
public interface FQService {

    @GET("v2/venues/search")
    Call<FoursquareResponse> venueSearch(
            @Header("Accept-Language") String lang
            , @Query("client_id") String client_id
            , @Query("client_secret") String client_secret
            , @Query("ll") String ll
            , @Query("limit") String limit
            , @Query("v") String version
            , @Query("m") String m

    );

    @GET("v2/venues/search")
    Call<FoursquareResponse> venueSearchKeyword(
            @Header("Accept-Language") String lang
            , @Query("client_id") String client_id
            , @Query("client_secret") String client_secret
            , @Query("ll") String ll
            , @Query("limit") String limit
            , @Query("query") String query
            , @Query("v") String version
            , @Query("m") String m

    );
}
