package com.rgb72.adventureearth.api;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by nutthawut on 8/28/16.
 */
public interface StickerCollectionService {

    @GET("api/v1/sticker-collections")
    Call<ShopItemResponse> request();

}
