package com.rgb72.adventureearth.api;

import android.util.Log;

import com.rgb72.adventureearth.model.foursquare.FoursquareResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nutthawut on 12/4/16.
 */
public class FoursquareHelper {

    private static FoursquareHelper instance;
    private Retrofit retrofit;

    private final String CLIENT_ID = "3MKXXACLJG5OCTBVGFZC5WJ3RUHDQZFSYHHTOIM3B0WTOSJV";
    private final String CLIENT_SECRET = "ZCVD2JMS0SFHA2BASTX1DD0CBW0DKVEDTTFUCYXXEQ1EHPI5";
    private final String API_VERSION = "20161204";

    public static FoursquareHelper getInstance() {
        if (instance == null) {
            instance = new FoursquareHelper();
        }
        return instance;
    }

    FoursquareHelper() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void getPlace(String latlng, final FoursquareCallback callback){

        FQService fqService = retrofit.create(FQService.class);
        Call<FoursquareResponse> call = fqService.venueSearch("en", CLIENT_ID, CLIENT_SECRET, latlng, "50", API_VERSION, "foursquare");
        call.enqueue(new Callback<FoursquareResponse>() {
            @Override
            public void onResponse(Call<FoursquareResponse> call, retrofit2.Response<FoursquareResponse> response) {

                if (response.body() == null) { return; }
                if (response.body().getResponse() == null) { return; }
                if (response.body().getResponse().getVenues() == null) { return; }

                callback.onSuccess(response.body().getResponse().getVenues());
            }

            @Override
            public void onFailure(Call<FoursquareResponse> call, Throwable t) {
                callback.onError();
            }
        });
    }

    public void getPlaceKeyword(String latlng, String keyword, final FoursquareCallback callback){

        FQService fqService = retrofit.create(FQService.class);
        Call<FoursquareResponse> call = fqService.venueSearchKeyword("en", CLIENT_ID, CLIENT_SECRET, latlng, "50", keyword, API_VERSION, "foursquare");
        call.enqueue(new Callback<FoursquareResponse>() {
            @Override
            public void onResponse(Call<FoursquareResponse> call, retrofit2.Response<FoursquareResponse> response) {

                if (response.body() == null) { return; }
                if (response.body().getResponse() == null) { return; }
                if (response.body().getResponse().getVenues() == null) { return; }

                callback.onSuccess(response.body().getResponse().getVenues());
            }

            @Override
            public void onFailure(Call<FoursquareResponse> call, Throwable t) {
                callback.onError();
            }
        });
    }
}
