package com.rgb72.adventureearth.fragment;

/**
 * Created by nutthawut on 8/28/16.
 */
public interface MyAdapterClickListener {
    void onItemClick(int position );
}
