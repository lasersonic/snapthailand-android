package com.rgb72.adventureearth.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rgb72.adventureearth.activities.FilterActivity;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.adapter.CollectionItemAdapter;
import com.rgb72.adventureearth.adapter.EffectItemAdapter;
import com.rgb72.adventureearth.adapter.FilterAdapter;
import com.rgb72.adventureearth.adapter.StickerAdapter;
import com.rgb72.adventureearth.adapter.StickerCategoryAdapter;
import com.rgb72.adventureearth.adapter.TextFontAdapter;
import com.rgb72.adventureearth.adapter.ViewColorAdapter;
import com.rgb72.adventureearth.custom.ItemSize;
import com.rgb72.adventureearth.manager.FilterManager;
import com.rgb72.adventureearth.manager.RealmHelper;
import com.rgb72.adventureearth.model.CollectionItem;
import com.rgb72.adventureearth.model.EditingText;
import com.rgb72.adventureearth.model.EffectItem;
import com.rgb72.adventureearth.model.FilterItem;
import com.rgb72.adventureearth.model.SubMenu;
import com.rgb72.adventureearth.model.TabMenu;
import com.rgb72.adventureearth.model.TextFontItem;
import com.rgb72.adventureearth.model.filter.FilterParam;
import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.model.realm.StickerObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;

public class RecycleViewFragment extends Fragment {

    private static final String TAB_MENU_ARGS = "TAB_MENU_ARGS";
    private static final String SUB_MENU_ARGS = "SUB_MENU_ARGS";
    private static final String EFFECT_ARGS   = "EFFECT_FILTER_ARGS";
    private static final String FILTER_THUMBS = "FILTER_THUMBS";

    @Bind(R.id.recyclerMenu_recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.btnMenuClose)
    ImageView btnMenuClose;
    @Bind(R.id.recycler_title_textView)
    TextView recyclerTitleTextView;
    @Bind(R.id.btnMenuOK)
    ImageView btnMenuOK;
    @Bind(R.id.recycler_header_layout)
    RelativeLayout recyclerHeaderLayout;

    private CollectionItemAdapter mAdapter;
    private ArrayList<StickerCollection> mStickerCollections;
    private List<EffectItem> mEffectLists = Arrays.asList(
            new EffectItem("CROP", R.drawable.crop, R.drawable.crop_active)
            , new EffectItem("ROTATE", R.drawable.rotate, R.drawable.rotate_active)
            , new EffectItem("BRIGHTNESS", R.drawable.brightness, R.drawable.brightness_active)
            , new EffectItem("CONTRAST", R.drawable.contrast, R.drawable.contrast_active)
            , new EffectItem("SATURATION", R.drawable.saturation, R.drawable.saturation_active)
            , new EffectItem("SHARPEN", R.drawable.sharpen, R.drawable.sharpen_active)
            , new EffectItem("FADE", R.drawable.fade, R.drawable.fade_active)
    );

    public static RecycleViewFragment newInstance(TabMenu tabMenu, FilterParam filterParam) {
        RecycleViewFragment fragment = new RecycleViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(TAB_MENU_ARGS, tabMenu);
        args.putParcelable(EFFECT_ARGS, filterParam);
        fragment.setArguments(args);
        return fragment;
    }

    public static RecycleViewFragment newInstance(TabMenu tabMenu, ArrayList<Bitmap> filteredThumbnails) {
        RecycleViewFragment fragment = new RecycleViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(TAB_MENU_ARGS, tabMenu);
        args.putSerializable(FILTER_THUMBS, filteredThumbnails);
        fragment.setArguments(args);
        return fragment;
    }

    public static RecycleViewFragment newInstance(SubMenu subMenu) {
        RecycleViewFragment fragment = new RecycleViewFragment();
        Bundle args = new Bundle();
        args.putSerializable(SUB_MENU_ARGS, subMenu);
        fragment.setArguments(args);
        return fragment;
    }

    public RecycleViewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_menu, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        TabMenu tabMenu = (TabMenu) getArguments().getSerializable(TAB_MENU_ARGS);
        SubMenu subMenu = (SubMenu) getArguments().getSerializable(SUB_MENU_ARGS);
        FilterParam filterParam = getArguments().getParcelable(EFFECT_ARGS);

        ArrayList<Bitmap> filteredThumbnails = (ArrayList<Bitmap>) getArguments().getSerializable(FILTER_THUMBS);

        if (tabMenu != null) {
            recyclerHeaderLayout.setVisibility(View.GONE);

            switch (tabMenu) {
                case FILTER:
                    if (filteredThumbnails != null) {
                        showFilterList(filteredThumbnails);
                    }
                    break;
                case EFFECT:
                    showEffectList(filterParam);
                    break;
                case TEXT:
                    showTextMenuList();
                    break;
                case STICKER:
                    showStickerCategoryList();
                    break;
                case LOCATION:
                    break;
                case FRAME:
                    showFrameList();
                    break;
            }
        }
        else if (subMenu != null) {
            switch (subMenu) {
                case FONT:
                    showFontList();
                    break;
                case FONT_COLOR:
                    showTextColorList();
                    break;
            }
        }
    }


    //
    // Showing List Methods.
    //

    private void showFilterList(ArrayList<Bitmap> filteredThumbnails) {
        FilterAdapter mAdapter = new FilterAdapter(getActivity(), FilterManager.getFilterItem(), filteredThumbnails);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.getPositionClicks().subscribe(new Action1<FilterItem>() {
            @Override
            public void call(FilterItem filterItem) {
                FilterActivity filterFragment = (FilterActivity) getActivity();
                if (filterFragment != null) {
                    filterFragment.onClickFilter(filterItem);
                }
            }
        });
    }

    private void showEffectList(FilterParam filterParam) {
        EffectItemAdapter mAdapter = new EffectItemAdapter(getActivity(), mEffectLists, filterParam);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.getPositionClicks().subscribe(new Action1<CollectionItem>() {
            @Override
            public void call(CollectionItem item) {
                FilterActivity filterFragment = (FilterActivity) getActivity();
                if (filterFragment != null) {
                    filterFragment.onClickEffectCollection(item);
                }
            }
        });
    }

    private void showTextMenuList() {
        List<CollectionItem> items = Arrays.asList(
                new CollectionItem("FONT", R.drawable.font)
                , new CollectionItem("COLOR", R.drawable.color)
                , new CollectionItem("OPACITY", R.drawable.fade)
        );

        mAdapter = new CollectionItemAdapter(getActivity(), items, ItemSize.SCREEN_RATIO);

        FilterActivity filterFragment = (FilterActivity) getActivity();
        if (filterFragment!=null) {
            mAdapter.setEnableLists(getEnableList(filterFragment.getActiveSticker() != null));
        }

        mRecyclerView.setAdapter(mAdapter);

        mAdapter.getPositionClicks().subscribe(new Action1<CollectionItem>() {
            @Override
            public void call(CollectionItem item) {

                FilterActivity filterFragment = (FilterActivity) getActivity();

                if (item.getSubMenu() == SubMenu.FONT) {
                    if (filterFragment != null) {
                        filterFragment.onClickShowFontList();
                    }
                } else if (item.getSubMenu() == SubMenu.FONT_COLOR) {
                    if (filterFragment != null) {
                        filterFragment.onClickShowFontColorList();
                    }
                } else if (item.getSubMenu() == SubMenu.FONT_OPACITY) {
                    if (filterFragment != null) {
                        filterFragment.onClickShowFontOpacity(item);
                    }
                }
            }
        });
    }

    private void showFontList() {

        recyclerHeaderLayout.setVisibility(View.VISIBLE);
        recyclerTitleTextView.setText("FONT");

        List<TextFontItem> items = Arrays.asList(
                new TextFontItem("DancingScriptOT.otf", 22)
                , new TextFontItem("FrederickatheGreat.ttf", 22)
                , new TextFontItem("Homestead.ttf", 22)
                , new TextFontItem("Lavanderia.otf", 18)
                , new TextFontItem("Limelight.ttf", 20)
                , new TextFontItem("Monoton.ttf", 20)
                , new TextFontItem("Orbitron.ttf", 20)
                , new TextFontItem("PoiretOne.ttf", 20)
                , new TextFontItem("Sniglet.ttf", 22)
                , new TextFontItem("WireOne.ttf", 24)
        );
        TextFontAdapter mAdapter = new TextFontAdapter(getActivity(), items);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.getPositionClicks().subscribe(new Action1<TextFontItem>() {
            @Override
            public void call(TextFontItem item) {
                FilterActivity filterFragment = (FilterActivity) getActivity();
                if (filterFragment != null) {
                    EditingText editingText = new EditingText();
                    editingText.setTypefaceName(item.getTypefaceName());
                    filterFragment.onClickAddTextFont(editingText);
                }
            }
        });
    }

    private void showTextColorList() {

        recyclerHeaderLayout.setVisibility(View.VISIBLE);
        recyclerTitleTextView.setText("COLOR");

        int[] textColorArray = getResources().getIntArray(R.array.textColorArray);
        ViewColorAdapter mAdapter = new ViewColorAdapter(getActivity(), textColorArray);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.getPositionClicks().subscribe(new Action1<Integer>() {
            @Override
            public void call(Integer colorResId) {
                FilterActivity filterFragment = (FilterActivity) getActivity();
                if (filterFragment != null) {
                    EditingText editingText = new EditingText();
                    editingText.setResTextColorId(colorResId);
                    filterFragment.onClickAddTextFont(editingText);
                }
            }
        });
    }

    private void showStickerCategoryList() {
        if (mStickerCollections == null) {
            mStickerCollections = RealmHelper.listStickerCollection(getActivity());
        }

        StickerCategoryAdapter mAdapter = new StickerCategoryAdapter(getActivity(), mStickerCollections);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.getPositionClicks().subscribe(new Action1<StickerCollection>() {
            @Override
            public void call(StickerCollection collection) {
                //If null mean press shopping cart.
                if (collection == null) {
                    ((FilterActivity) getActivity()).goToShop();
                } else {
                    showStickerItemList(collection.getStickerLists());
                }
            }
        });
    }

    private void showStickerItemList(ArrayList<StickerObject> stickerObjectArrayList) {
        StickerAdapter mAdapter = new StickerAdapter(getActivity(), stickerObjectArrayList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.getPositionClicks().subscribe(new Action1<StickerObject>() {
            @Override
            public void call(StickerObject stickerObject) {
                FilterActivity filterFragment = (FilterActivity) getActivity();
                //If null mean press shopping cart.
                if (stickerObject == null) {
                    showStickerCategoryList();
                }
                else if (filterFragment != null) {
                    filterFragment.onClickAddSticker(stickerObject);
                }
            }
        });
    }


    private void showFrameList() {
        List<CollectionItem> items = Arrays.asList(
                new CollectionItem("None", R.drawable.frame_1)
                , new CollectionItem("WHITE", R.drawable.frame_2)
                , new CollectionItem("BLACK", R.drawable.frame_3)
                , new CollectionItem("POSTCARD", R.drawable.frame_4)
                , new CollectionItem("MOVIE", R.drawable.frame_5)
                , new CollectionItem("POLAROID", R.drawable.frame_6)
        );
        CollectionItemAdapter mAdapter = new CollectionItemAdapter(getActivity(), items, ItemSize.SCREEN_RATIO);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.getPositionClicks().subscribe(new Action1<CollectionItem>() {
            @Override
            public void call(CollectionItem item) {
                FilterActivity filterFragment = (FilterActivity) getActivity();
                if (filterFragment != null) {
                    filterFragment.onClickFrame(item);
                }
            }

        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.btnMenuClose, R.id.btnMenuOK})
    public void onClick(View view) {

        FilterActivity filterFragment = (FilterActivity) getActivity();

        switch (view.getId()) {
            case R.id.btnMenuClose:
                if (filterFragment != null) {
                    filterFragment.onMenuClose();
                }
                break;
            case R.id.btnMenuOK:
                if (filterFragment != null) {
                    filterFragment.onMenuOK();
                }
                break;
        }
    }


    public void setEnableFontColor(boolean enabled){
        if (mAdapter == null) {
            return;
        }
        mAdapter.setEnableLists(getEnableList(enabled));
        mAdapter.notifyItemRangeChanged(1,2);
    }

    private boolean[] getEnableList(boolean enabled){
        if (enabled) {
            return new boolean[]{true, true, true};
        }else {
            return new boolean[]{true, false, false};
        }
    }
}
