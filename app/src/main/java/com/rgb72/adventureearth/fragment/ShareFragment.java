package com.rgb72.adventureearth.fragment;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.manager.AnswersManager;
import com.rgb72.adventureearth.utils.DialogUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ShareFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_BITMAP = "ARG_BITMAP";

    private final String SHARE_SUPPORT_EMAIL = "support@rgb72.net";
    private final String SHARE_MESSAGE = "#adventureearth";
    private final String SHARE_SUBJECT = "";


    @Bind(R.id.share_facebook_layout)
    RelativeLayout shareFacebookLayout;
    @Bind(R.id.share_instagram_layout)
    RelativeLayout shareInstagramLayout;
    @Bind(R.id.share_twitter_layout)
    RelativeLayout shareTwitterLayout;
    @Bind(R.id.share_mail_layout)
    RelativeLayout shareMailLayout;
    @Bind(R.id.share_save_layout)
    RelativeLayout shareSaveLayout;
    @Bind(R.id.share_more_layout)
    RelativeLayout shareMoreLayout;
    @Bind(R.id.share_close_button)
    ImageView mCloseButton;
    @Bind(R.id.share_blurBG_image)
    ImageView shareBlurBGImage;


    private Bitmap mBitmap;

    private ViewGroup mContainer;

    public static ShareFragment newInstance(Bitmap image) {
        ShareFragment fragment = new ShareFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_BITMAP, image);
        fragment.setArguments(args);
        return fragment;
    }

    public ShareFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mBitmap = getArguments().getParcelable(ARG_BITMAP);
        }

        saveImageToCache();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = (ViewGroup) container.getParent();
        View view = inflater.inflate(R.layout.fragment_share, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    private void shareFacebook() {
        AnswersManager.logUserSelectShareSocial("facebook");

        ShareDialog shareDialog = new ShareDialog(this);

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareHashtag shareHashtag = new ShareHashtag.Builder()
                    .setHashtag(SHARE_MESSAGE)
                    .build();
            SharePhoto sharePhoto = new SharePhoto.Builder()
                    .setImageUrl(getUriImageCache())
                    .build();
            ShareContent shareContent = new ShareMediaContent.Builder()
                    .setShareHashtag(shareHashtag)
                    .addMedium(sharePhoto)
                    .build();
            shareDialog.show(shareContent);
        } else {
            shareMore();
        }

    }

    private void shareInstagram() {
        if (mBitmap == null) {
            return;
        }
        AnswersManager.logUserSelectShareSocial("instagram");
        Uri contentUri = getUriImageCache();
        String type = "image/*";
        createInstagramIntent(type, contentUri);
    }

    private void createInstagramIntent(String type, Uri uri) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setPackage("com.instagram.android");
        share.setType(type);
        share.putExtra(Intent.EXTRA_STREAM, uri);

        PackageManager packManager = getActivity().getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(share, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.instagram.android")) {
                share.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }

        if (resolved) {
            startActivity(share);
        } else {
            Toast.makeText(getActivity(), "Instagram App is not installed", Toast.LENGTH_LONG).show();
        }
    }

    private void shareTwitter() {
        AnswersManager.logUserSelectShareSocial("twitter");
        shareMore();
    }

    private void shareEmail() {
        if (mBitmap == null) {
            return;
        }

        AnswersManager.logUserSelectShareSocial("mail");

        Uri contentUri = getUriImageCache();

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, SHARE_SUPPORT_EMAIL);
        emailIntent.putExtra(Intent.EXTRA_TEXT, SHARE_MESSAGE);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, SHARE_SUBJECT);
        emailIntent.setType("application/image");
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        emailIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
        startActivity(emailIntent);
    }

    private void saveToCameraRoll() {
        String fileName = String.format("save_%d.png", System.currentTimeMillis());
        saveImageToGallery(fileName);
        AnswersManager.logUserSelectShareSocial("cameraroll");
    }

    private void shareMore() {
        if (mBitmap == null) {
            return;
        }
        AnswersManager.logUserSelectShareSocial("more");

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        share.putExtra(Intent.EXTRA_TEXT, SHARE_MESSAGE);
        share.putExtra(Intent.EXTRA_SUBJECT, SHARE_SUBJECT);
        share.putExtra(Intent.EXTRA_STREAM, getUriImageCache());
        startActivity(Intent.createChooser(share, "Share to"));
    }

    private void saveImageToCache() {
        try {
            File cachePath = new File(getActivity().getCacheDir(), "images");
            if (!cachePath.exists()) {
                cachePath.mkdirs(); // don't forget to make the directory
            }
            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Uri getUriImageCache() {
        File imagePath = new File(getActivity().getCacheDir(), "images");
        File newFile = new File(imagePath, "image.png");
        Uri contentUri = FileProvider.getUriForFile(getActivity(), "com.rgb72.adventureearth.fileprovider", newFile);
        return contentUri;
    }

    private boolean saveImageToGallery(String fileName) {

        if (mBitmap == null) {
            return false;
        }

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/AdventureEarth"); //Creates app specific folder
        if (!path.exists()) {
            path.mkdirs();
        }


        File imageFile = new File(path, fileName);

        try {
            FileOutputStream out = new FileOutputStream(imageFile);
            boolean success = mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // Compress Image
            out.flush();
            out.close();

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(getActivity(), new String[]{imageFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
            });


            if (success) {
                DialogUtils.showMessageCancel(getActivity(), "Save to gallery success.", null);
            }

            return success;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @OnClick({R.id.share_close_button, R.id.share_facebook_layout, R.id.share_instagram_layout, R.id.share_twitter_layout, R.id.share_mail_layout, R.id.share_save_layout, R.id.share_more_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.share_close_button:
                getActivity().onBackPressed();
                break;
            case R.id.share_facebook_layout:
                shareFacebook();
                break;
            case R.id.share_instagram_layout:
                shareInstagram();
                break;
            case R.id.share_twitter_layout:
                shareTwitter();
                break;
            case R.id.share_mail_layout:
                shareEmail();
                break;
            case R.id.share_save_layout:
                saveToCameraRoll();
                break;
            case R.id.share_more_layout:
                shareMore();
                break;
        }
    }
}
