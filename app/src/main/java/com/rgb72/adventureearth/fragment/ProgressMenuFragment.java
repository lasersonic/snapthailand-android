package com.rgb72.adventureearth.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.activities.FilterActivity;
import com.rgb72.adventureearth.custom.ProgressMenuType;
import com.rgb72.adventureearth.model.CollectionItem;
import com.rgb72.adventureearth.model.SubMenu;
import com.rgb72.adventureearth.views.BubbleView;
import com.rgb72.adventureearth.views.StartPointSeekBar;

public class ProgressMenuFragment extends Fragment {

    private static final String ARG_PARAM1 = "MENU_TYPE";
    private static final String ARG_PARAM2 = "SUB_MENU";
    private static final String ARG_PARAM3 = "SUB_MENU_VALUE";

    public interface MenuCallback {
        void onMenuClose();
        void onMenuOK();
        void onRotateLeft();
        void onRotateRight();
    }

    private MenuCallback menuCallback;
    private CollectionItem mItem;
    private SubMenu mSubMenu;
    private BubbleView mBubbleView;
    private StartPointSeekBar mSeekbar;
    private double mValue;

    public static ProgressMenuFragment newInstance(CollectionItem item, SubMenu subMenu, double subMenuEffectValue) {
        ProgressMenuFragment fragment = new ProgressMenuFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, item);
        args.putSerializable(ARG_PARAM2, subMenu);
        args.putSerializable(ARG_PARAM3, subMenuEffectValue);

        fragment.setArguments(args);
        return fragment;
    }

    public ProgressMenuFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItem = (CollectionItem) getArguments().getSerializable(ARG_PARAM1);
            mSubMenu = (SubMenu) getArguments().getSerializable(ARG_PARAM2);
            mValue = getArguments().getDouble(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ProgressMenuType menuType = getProgressMenuType(mSubMenu);

        View view;

        if (menuType == ProgressMenuType.PROGRESS) {
            view = inflater.inflate(R.layout.fragment_progress_menu, container, false);
            TextView textView = (TextView)view.findViewById(R.id.textviewProgressTitle);
            textView.setText(mItem.getItemName().toUpperCase());
        } else if (menuType == ProgressMenuType.OK_CANCEL){
            view = inflater.inflate(R.layout.fragment_ok_cancel_menu, container, false);
            TextView textView = (TextView)view.findViewById(R.id.textviewProgressTitle);
            textView.setText(mItem.getItemName().toUpperCase());
        }
        else {
            view = inflater.inflate(R.layout.fragment_rotate_menu, container, false);

            ImageView leftButton = (ImageView)view.findViewById(R.id.btnRotateLeft);
            ImageView rightButton = (ImageView)view.findViewById(R.id.btnRotateRight);
            leftButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    menuCallback.onRotateLeft();
                }
            });
            rightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    menuCallback.onRotateRight();
                }
            });

        }

        ImageView closeImageView = (ImageView)view.findViewById(R.id.btnMenuClose);
        ImageView okImageView = (ImageView)view.findViewById(R.id.btnMenuOK);
        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuCallback.onMenuClose();
            }
        });
        okImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuCallback.onMenuOK();
            }
        });

        if (menuType == ProgressMenuType.PROGRESS) {
            mBubbleView = (BubbleView) view.findViewById(R.id.progress_bubble_view);
            mBubbleView.setAlpha(0);
            mSeekbar = (StartPointSeekBar) view.findViewById(R.id.startPointSeekBar);
            setSeekbarMinMaxValue(mSeekbar, mSubMenu);
            mSeekbar.setProgress(mValue);
            mSeekbar.setOnSeekBarChangeListener(new StartPointSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onOnSeekBarTouchDownOrUp(StartPointSeekBar bar, float thumbPositionX, boolean touchDown) {

                    mBubbleView.setPositionX(thumbPositionX);
                    mBubbleView.animate().alpha(touchDown ? 1 : 0).setDuration(300).start();

                    if (!touchDown) {
                        FilterActivity filterActivity = (FilterActivity) getActivity();
                        if (filterActivity != null) {
                            filterActivity.setEffectValue(mValue);
                        }
                    }
                }

                @Override
                public void onOnSeekBarValueChange(StartPointSeekBar bar, float thumbPositionX, double value) {
                    mValue = value;
                    mBubbleView.setPositionX(thumbPositionX);
                    mBubbleView.setProgressValue(String.valueOf((int) value));
                    mBubbleView.invalidate();
                }
            });
        }

        return view;

    }

    private ProgressMenuType getProgressMenuType(SubMenu mSubMenu) {
        if (mSubMenu == SubMenu.CROP || mSubMenu == SubMenu.LOCATION) {
            return ProgressMenuType.OK_CANCEL;
        }
        else if (mSubMenu == SubMenu.ROTATE) {
            return ProgressMenuType.ROTATE;
        }
        else {
            return ProgressMenuType.PROGRESS;
        }
    }

    private void setSeekbarMinMaxValue(StartPointSeekBar seekBar, SubMenu subMenu) {
        switch (subMenu) {
            case FADE :
            case FONT_OPACITY:
            case INTENSITY:
                seekBar.setAbsoluteMinMaxValue(0, 100);
                break;
            default:
                seekBar.setAbsoluteMinMaxValue(-50, 50);
                break;
        }

    }

    public void setMenuCallback(MenuCallback menuCallback) {
        this.menuCallback = menuCallback;
    }

}
