package com.rgb72.adventureearth.custom;

import android.app.Application;

import com.rgb72.adventureearth.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by nutthawut on 9/11/16.
 */
public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
//        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/avenirlight.ttf");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/avenirlight.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}

