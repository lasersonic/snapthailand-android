package com.rgb72.adventureearth.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.adapter.ShopDetailAdapter;
import com.rgb72.adventureearth.api.API;
import com.rgb72.adventureearth.api.DownloadProgressInterceptor;
import com.rgb72.adventureearth.manager.AnswersManager;
import com.rgb72.adventureearth.manager.InAppWrapper;
import com.rgb72.adventureearth.manager.RealmHelper;
import com.rgb72.adventureearth.manager.UnzipManager;
import com.rgb72.adventureearth.model.ShopItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.functions.Action1;


public class ShopDetailActivity extends BaseActivity {

    private static final String ARG_PARAM1 = "ShopItem";

    @Bind(R.id.navTitle)
    TextView navTitle;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;


    private InAppWrapper inAppWrapper;

    private ShopItem shopItem;
    private ShopDetailAdapter shopDetailAdapter;

    private long timestamp = 0;

    public static void openActivity(Context pContext, ShopItem item) {
        Intent intent = new Intent(pContext, ShopDetailActivity.class);
        intent.putExtra(ARG_PARAM1, item);
        pContext.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_shop);
        ButterKnife.bind(this);

        navTitle.setText(getString(R.string.action_shop_detail));
        inAppWrapper = new InAppWrapper(this);

        if (getIntent().getSerializableExtra(ARG_PARAM1) != null) {
            shopItem = (ShopItem)getIntent().getSerializableExtra(ARG_PARAM1);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        shopDetailAdapter = new ShopDetailAdapter(this, shopItem);
        recyclerView.setAdapter(shopDetailAdapter);

        //User click buy
        shopDetailAdapter.getBuyClick().subscribe(new Action1<Void>() {
            @Override
            public void call(Void na) {

                if (shopItem.getPrice() > 0) {
                    purchase(shopItem);
                }
                else {
                    download(shopItem);
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!inAppWrapper.getBp().handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        ButterKnife.unbind(this);
        if (inAppWrapper != null) {
            inAppWrapper.release();
        }
        super.onDestroy();
    }

    private void purchase(final ShopItem item) {

        boolean isAvailable = BillingProcessor.isIabServiceAvailable(this);

        if(!isAvailable) {
            Toast.makeText(getApplicationContext(), R.string.purchase_error, Toast.LENGTH_SHORT).show();
        }

        inAppWrapper.purchase(item, new InAppWrapper.InAppWrapperPurchaseCallback() {
            @Override
            public void onPurchaseSuccess() {
                AnswersManager.logUserBuySticker(item.getAndroid_inapp_id());
                sendPurchased(item);
                download(item);
            }

            @Override
            public void onPurchaseFail(Throwable error) {
                Toast.makeText(getApplicationContext(), R.string.purchase_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void download(final ShopItem item) {

        showLoadingProgress();

        API.getInstance(this).downloadSticker(item, new DownloadProgressInterceptor.DownloadProgressListener() {
            @Override
            public void update(long bytesRead, long contentLength, final boolean done) {
                if (!done) {
                    updateProgress(bytesRead, contentLength);
                }
            }

        }, new UnzipManager.UnzipManagerCallback() {
            @Override
            public void unzipSuccess() {

                shopDetailAdapter.setmStickerCollections(RealmHelper.listStickerCollection(ShopDetailActivity.this));
                shopDetailAdapter.notifyItemChanged(0);

                hideLoading();
            }

            @Override
            public void unzipFail(Exception e) {
                hideLoading();
            }
        });
    }

    private void updateProgress(final long bytesRead, final long contentLength) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                long currentTime = System.currentTimeMillis();

                if (currentTime - timestamp > 2000) {
                    timestamp = currentTime;
                    int progress = (int) (((float) bytesRead / (float) contentLength) * 100);
                    setLoadingProgress(progress);
                }
            }
        });
    }

    private void sendPurchased(final ShopItem item){
        API.getInstance(this).sendPurchased(item, new API.CallbackRequest() {
            @Override
            public void onSuccess(boolean success) {
                Log.d("nut", "sendPurchased = " + (success ? "success" : "fail"));
            }
        });
    }
}
