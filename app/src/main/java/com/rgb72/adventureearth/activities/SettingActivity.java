package com.rgb72.adventureearth.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.adapter.SettingAdapter;
import com.rgb72.adventureearth.manager.InAppWrapper;
import com.rgb72.adventureearth.utils.DialogUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.functions.Action1;

public class SettingActivity extends BaseActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    public static void openActivity(Context pContext) {
        pContext.startActivity(new Intent(pContext, SettingActivity.class));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_setting);
        ButterKnife.bind(this);

        SettingAdapter mAdapter = new SettingAdapter(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        Subscription sub = mAdapter.getPositionClicks().subscribe(new Action1<Integer>() {
            @Override
            public void call(Integer position) {
                Log.d("nut", "onClick " + position);
                handlerOnClick(position);
            }
        });

    }

    void handlerOnClick(int position) {

        //0 and 3 is section

        if (position == 1) {
            //Go to sticker shop
            goToShop();
        }
        if (position == 2) {
            //Restore purchase
            showLoading();

            boolean restoreResult = new InAppWrapper(this).restore(new InAppWrapper.InAppWrapperRestoreCallback() {
                @Override
                public void onRestoreSuccess() {
                    hideLoading();
                    DialogUtils.showMessageOK(SettingActivity.this, getString(R.string.restore_success));
                }
            });

            if (!restoreResult) {
                DialogUtils.showMessageOK(SettingActivity.this, getString(R.string.restore_fail));
                hideLoading();
            }
        }
        if (position == 4) {
            //Send feedback
            this.sendEmail();
        }
        if (position == 5) {
            //Rate this app
            this.rateThisApp();
        }
    }

    public void goToShop() {
        ShopActivity.openActivity(this);
    }

    private void sendEmail() {
        final Intent emailIntent = new Intent(Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@rgb72.net"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "#snapthailand");

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    private void rateThisApp() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

}
