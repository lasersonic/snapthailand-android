package com.rgb72.adventureearth.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.desmond.squarecamera.ImageParameters;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;
import com.knef.stickerview.StickerImageView;
import com.knef.stickerview.StickerView;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.adapter.FramePagerAdapter;
import com.rgb72.adventureearth.api.FoursquareCallback;
import com.rgb72.adventureearth.api.FoursquareHelper;
import com.rgb72.adventureearth.manager.AnswersManager;
import com.rgb72.adventureearth.manager.NTSize;
import com.rgb72.adventureearth.model.filter.FilterParam;
import com.rgb72.adventureearth.utils.ImageOrientation;
import com.rgb72.adventureearth.views.CustomFrameView;
import com.rgb72.adventureearth.views.LocationSelectorView;
import com.rgb72.adventureearth.views.NTCropImageView;
import com.rgb72.adventureearth.fragment.ProgressMenuFragment;
import com.rgb72.adventureearth.fragment.RecycleViewFragment;
import com.rgb72.adventureearth.fragment.ShareFragment;
import com.rgb72.adventureearth.manager.FilterManager;
import com.rgb72.adventureearth.model.CollectionItem;
import com.rgb72.adventureearth.model.EditingText;
import com.rgb72.adventureearth.model.FilterItem;
import com.rgb72.adventureearth.model.SubMenu;
import com.rgb72.adventureearth.model.TabMenu;
import com.rgb72.adventureearth.model.foursquare.FoursquareVenues;
import com.rgb72.adventureearth.model.realm.StickerObject;
import com.rgb72.adventureearth.utils.DialogUtils;
import com.rgb72.adventureearth.manager.ImageManager;
import com.rgb72.adventureearth.utils.Utils;
import com.rgb72.adventureearth.views.StickerViewTouchHelper;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static android.R.attr.width;


public class FilterActivity extends BaseActivity implements ProgressMenuFragment.MenuCallback {

    private static final int ANIMATION_DURATION = 100;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS = 3943;
    private static final String ARG_PARAM_URI = "paramUri";
    private static final String ARG_PARAM_BITMAP = "paramBitmap";

    @Bind(R.id.filter_blurBG_imageView)
    ImageView filter_blurBG_imageView;
    @Bind(R.id.imageView)
    NTCropImageView imageView;
    @Bind(R.id.btnBack)
    Button btnBack;
    @Bind(R.id.btnNext)
    TextView btnNext;
    @Bind(R.id.progressViewLayoutForFragment)
    FrameLayout progressViewLayoutForFragment;
    @Bind(R.id.filter_image)
    ImageView filterImage;
    @Bind(R.id.filter_textView)
    TextView filterTextView;
    @Bind(R.id.filter_layout)
    RelativeLayout filterLayout;
    @Bind(R.id.effect_image)
    ImageView effectImage;
    @Bind(R.id.effect_textView)
    TextView effectTextView;
    @Bind(R.id.effect_layout)
    RelativeLayout effectLayout;
    @Bind(R.id.sticker_image)
    ImageView stickerImage;
    @Bind(R.id.sticker_textView)
    TextView stickerTextView;
    @Bind(R.id.sticker_layout)
    RelativeLayout stickerLayout;
    @Bind(R.id.text_image)
    ImageView textImage;
    @Bind(R.id.text_textView)
    TextView textTextView;
    @Bind(R.id.text_layout)
    RelativeLayout textLayout;
    @Bind(R.id.location_image)
    ImageView locationImage;
    @Bind(R.id.location_textView)
    TextView locationTextView;
    @Bind(R.id.location_layout)
    RelativeLayout locationLayout;
    @Bind(R.id.rootView)
    RelativeLayout rootView;
    @Bind(R.id.frame_image)
    ImageView frameImage;
    @Bind(R.id.frame_textView)
    TextView frameTextView;
    @Bind(R.id.frame_layout)
    RelativeLayout frameLayout;
    @Bind(R.id.filter_imageContainer_layout)
    FrameLayout filter_imageContainer_layout;
    @Bind(R.id.filter_shareFragment_layout)
    FrameLayout filter_shareFragment_layout;

    private Uri imageUri;

    final ArrayList<Bitmap> arrayFilteredBitmap = new ArrayList<>();
    final ArrayList<FilterItem> listFilter = FilterManager.getFilterItem();

    private SubMenu currentSubMenu;
    private TabMenu currentTabMenu;

    private StickerImageView activeSticker;

    //Location Stuff
    private ViewPager mViewpagerLocation;
    private View mCurrentLocationView;
    private int mCurrentLocationFrameIndex = 0;
    private String mCurrentLatLng;

    //Frame
    private CollectionItem mCurrentCollectionItemFrame;
    private CustomFrameView mCurrentFrame;

    //Rotate
    int oldRotate = 0;
    int newRotate = 0;

    //Temp for filter, effected but user cancel.
    private FilterParam mFilterParam = new FilterParam();
    private FilterParam mTempParam = new FilterParam();
    private FilterItem mCurrentFilterItem;

    private View okCancelStickerView;
    private LocationSelectorView mLocationSelectorView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param imageUri - Image Uri from Gallery select or photo.?.
     * @return A new instance of fragment FilterFragment.
     */
    public static void openActivity(Context pContext, Uri imageUri) {
        Intent intent = new Intent(pContext, FilterActivity.class);
        intent.putExtra(ARG_PARAM_URI, imageUri);
        pContext.startActivity(intent);
    }

    public FilterActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_filter);
        ButterKnife.bind(this);

        if (getIntent() != null) {

            Uri uri = getIntent().getParcelableExtra(ARG_PARAM_URI);
            if (uri != null) {
                this.imageUri = uri;

                Bitmap compressedBm = ImageManager.decodeSampledBitmapFromResource(this, imageUri, 500, 500);

                try {
                    //Fix orientation
                    Bitmap bitmapNew = ImageOrientation.modifyOrientation(getApplicationContext(), compressedBm, uri);

                    imageUri = uriFromCache("mainImage.png");

                    FileOutputStream stream = new FileOutputStream(imageUri.getPath()); // overwrites this image every time
                    bitmapNew.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    stream.close();

                    imageView.startLoad(this.imageUri, null);
                    filter_blurBG_imageView.setImageURI(this.imageUri);
                    initialValue();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (getIntent().getParcelableExtra(ARG_PARAM_BITMAP) != null) {
                Bitmap bm = getIntent().getParcelableExtra(ARG_PARAM_BITMAP);
                imageView.setImageBitmap(bm);
                filter_blurBG_imageView.setImageBitmap(bm);
            }
        }
    }

    public void initialValue() {
        imageView.setCropMode(CropImageView.CropMode.FREE);
        hideCropMode();
        setupOKCancelStickerView();
        processFilterThumbnails();
    }

    private void setupOKCancelStickerView() {
        okCancelStickerView = LayoutInflater.from(this).inflate(R.layout.fragment_ok_cancel_view, null, false);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.ABOVE, R.id.progressViewLayoutForFragment);
        okCancelStickerView.setLayoutParams(param);
        okCancelStickerView.setVisibility(View.INVISIBLE);
        okCancelStickerView.findViewById(R.id.btnMenuClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteActiveSticker();
            }
        });

        okCancelStickerView.findViewById(R.id.btnMenuOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentSubMenu != null && (currentSubMenu == SubMenu.FONT || currentSubMenu == SubMenu.FONT_COLOR || currentSubMenu == SubMenu.FONT_OPACITY)) {
                    onMenuOK();
                }
                clearActiveSticker();
            }
        });

        rootView.addView(okCancelStickerView);
    }

    private void removeOKCancelStickerView() {
        if (okCancelStickerView != null) {
            okCancelStickerView.setVisibility(View.INVISIBLE);
        }
    }

    private void hideCropMode() {
        animateCropViewPadding(false);
        imageView.setHandleShowMode(CropImageView.ShowMode.NOT_SHOW);
        imageView.setGuideShowMode(CropImageView.ShowMode.NOT_SHOW);
        imageView.setCropEnabled(false);
    }

    private void showCropMode() {
        animateCropViewPadding(true);
        imageView.setGuideShowMode(CropImageView.ShowMode.SHOW_ALWAYS);
        imageView.setHandleShowMode(CropImageView.ShowMode.SHOW_ALWAYS);
        imageView.setCropEnabled(true);
    }

    private void animateCropViewPadding(final boolean showCrop) {
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                int padding = Utils.convertDpToPixel(showCrop ? 15 : 0, FilterActivity.this);
                //FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)imageView.getLayoutParams();
                imageView.setPadding(padding, padding, padding, padding);
            }
        };
        animation.setDuration(1000); // in ms
        imageView.startAnimation(animation);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void processFilterThumbnails() {

        new Thread(new Runnable() {
            @Override
            public void run() {

            //Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            //Bitmap resizedBitmap = FilterManager.scaleDown(bitmap, 60, true);
            Bitmap resizedBitmap = ImageManager.decodeSampledBitmapFromResource(FilterActivity.this, imageUri, 100, 100);

            for (final FilterItem filter : listFilter) {
                FilterManager.getInstance().appliedFilter(FilterActivity.this, filter, resizedBitmap, new FilterManager.FilterManagerCallback() {
                    @Override
                    public void filteredImage(Bitmap filterBitmap) {
                        arrayFilteredBitmap.add(filterBitmap);
                    }
                });
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (currentTabMenu == null && !isFinishing()) {
                        filterAction();
                    }
                }
            });

            }

        }).start();
    }

    private Uri uriFromCache(String imageName) {
        File cachePath = new File(getCacheDir(), "images");
        if (!cachePath.exists()) {
            cachePath.mkdirs();
        }
        File file = new File(cachePath, imageName);
        if (file == null) {
            return null;
        }
        Uri target = Uri.fromFile(file);
        return target;
    }

    private void openShare() {

        clearActiveSticker();

        Rect imageRect = getRectSizeOfImage();

        Bitmap bitmap = Bitmap.createBitmap(filter_imageContainer_layout.getWidth(), filter_imageContainer_layout.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        filter_imageContainer_layout.draw(canvas);


        if (bitmap != null) {
            try {
                Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, Math.max(imageRect.left, 0), Math.max(imageRect.top, 0), Math.min(bitmap.getWidth(), imageRect.width()), Math.min(bitmap.getHeight(), imageRect.height()));
                bitmap = null;
                ShareFragment shareFragment = ShareFragment.newInstance(croppedBitmap);
                addFragment(R.id.filter_shareFragment_layout, shareFragment, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //==================================
    //Rotate
    //==================================
    private void reverseRotateImage(int rotateDiff, int duration) {
        rotateImage(-rotateDiff, duration);
    }

    private void rotateImage(int rotation, int duration) {
        switch (rotation) {
            case 90:
                imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D, duration);
                break;
            case 180:
                imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_180D, duration);
                break;
            case 270:
                imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_270D, duration);
                break;
            case -90:
                imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D, duration);
                break;
            case -180:
                imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M180D, duration);
                break;
            case -270:
                imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M270D, duration);
                break;
        }
    }

    private void doCropImage() {

        Uri target = uriFromCache("crop.png");
        imageView.startCrop(target, new CropCallback() {
            @Override
            public void onSuccess(Bitmap bitmap) {
                Log.d("nut", "crop success");
            }

            @Override
            public void onError() {
                Log.d("nut", "crop fail");

            }
        }, new SaveCallback() {
            @Override
            public void onSuccess(Uri uri) {
                Log.d("nut", "save success");
                imageUri = uri;
                imageView.startLoad(uri, new LoadCallback() {
                    @Override
                    public void onSuccess() {
                        oldRotate = 0;
                        newRotate = 0;
                        showLocationAndFrameView(true);
                        //re effect to original.
                        filterImage(mFilterParam);
                    }

                    @Override
                    public void onError() {

                    }
                });

            }

            @Override
            public void onError() {
                Log.d("nut", "save fail");
            }
        });
    }


    //********************
    // Filter Image
    //********************
    private void filterImage(FilterParam pFilterParam) {

        if (currentSubMenu == SubMenu.FADE) {
            imageView.setAlpha(pFilterParam.getFade());
            return;
        }

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), this.imageUri);
            FilterManager.getInstance().doFilter(this, bitmap, pFilterParam, new FilterManager.FilterManagerCallback() {
                @Override
                public void filteredImage(Bitmap filterBitmap) {
                    imageView.setImageBitmap(filterBitmap);
                    rotateImage(oldRotate, 0);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //===========================================
    // Go to shop
    //===========================================

    public void goToShop() {
        ShopActivity.startActivityForResult(this);
    }


    //=============================

    /**
     * Menu Action
     */
    //=============================
    private void filterAction() {
        if (currentTabMenu == TabMenu.FILTER) {
            return;
        }
        if (currentSubMenu != null) {
            onMenuClose();
        }
        clearActiveSticker();
        clearActiveSelectedMenu();
        filterImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter_active));
        filterTextView.setTextColor(ContextCompat.getColor(this, R.color.activeText));
        currentTabMenu = TabMenu.FILTER;
        RecycleViewFragment recycleViewFragment = RecycleViewFragment.newInstance(currentTabMenu, arrayFilteredBitmap);
        replaceFragment(R.id.progressViewLayoutForFragment, recycleViewFragment);
    }

    private void effectAction() {
        if (currentTabMenu == TabMenu.EFFECT) {
            return;
        }
        if (currentSubMenu != null) {
            onMenuClose();
        }
        clearActiveSticker();
        clearActiveSelectedMenu();
        effectImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.effect_active));
        effectTextView.setTextColor(ContextCompat.getColor(this, R.color.activeText));
        currentTabMenu = TabMenu.EFFECT;
        showRecyclerViewMenu(currentTabMenu);
    }

    private void textAction() {
        if (currentTabMenu == TabMenu.TEXT) {
            return;
        }
        if (currentSubMenu != null) {
            onMenuClose();
        }
        clearActiveSticker();
        clearActiveSelectedMenu();
        textImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.text_active));
        textTextView.setTextColor(ContextCompat.getColor(this, R.color.activeText));
        currentTabMenu = TabMenu.TEXT;
        showRecyclerViewMenu(currentTabMenu);
    }

    private void enableTextFontAndColor(boolean enable) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.progressViewLayoutForFragment);
        if (fragment instanceof RecycleViewFragment) {
            ((RecycleViewFragment) fragment).setEnableFontColor(enable);
        }
    }

    private void stickerAction() {
        if (currentTabMenu == TabMenu.STICKER) {
            return;
        }
        if (currentSubMenu != null) {
            onMenuClose();
        }
        clearActiveSticker();
        clearActiveSelectedMenu();
        stickerImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.sticker_active));
        stickerTextView.setTextColor(ContextCompat.getColor(this, R.color.activeText));
        currentTabMenu = TabMenu.STICKER;
        showRecyclerViewMenu(currentTabMenu);
    }

    private void locationAction() {
        if (currentTabMenu == TabMenu.LOCATION) {
            return;
        }

        clearActiveSticker();

        if (currentSubMenu != null) {
            onMenuClose();
        }
        if (checkLocationPermission()) {
            clearActiveSelectedMenu();

            locationImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.location_active));
            locationTextView.setTextColor(ContextCompat.getColor(this, R.color.activeText));
            currentTabMenu = TabMenu.LOCATION;
            currentSubMenu = SubMenu.LOCATION;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    ProgressMenuFragment fragment = ProgressMenuFragment.newInstance(new CollectionItem("Location", 0), SubMenu.LOCATION, 0);
                    fragment.setMenuCallback(FilterActivity.this);
                    replaceFragment(R.id.progressViewLayoutForFragment, fragment);

                    getCurrentLocation();

                }
            }, 400);
        }
    }

    private void frameAction() {
        if (currentTabMenu == TabMenu.FRAME) {
            return;
        }
        if (currentSubMenu != null) {
            onMenuClose();
        }
        clearActiveSticker();
        clearActiveSelectedMenu();
        frameImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.frame_active));
        frameTextView.setTextColor(ContextCompat.getColor(this, R.color.activeText));
        currentTabMenu = TabMenu.FRAME;
        showRecyclerViewMenu(currentTabMenu);
    }

    //=============================
    // Show Fragment Menu
    //=============================

    private void showRecyclerViewMenu(TabMenu tabMenu) {
        replaceFragment(R.id.progressViewLayoutForFragment, RecycleViewFragment.newInstance(tabMenu, mFilterParam));
    }

    private void clearActiveSelectedMenu() {
        filterImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.filter));
        effectImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.effect));
        stickerImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.sticker));
        textImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.text));
        locationImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.location));
        frameImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.frame));

        filterTextView.setTextColor(ContextCompat.getColor(this, R.color.inactiveText));
        effectTextView.setTextColor(ContextCompat.getColor(this, R.color.inactiveText));
        stickerTextView.setTextColor(ContextCompat.getColor(this, R.color.inactiveText));
        textTextView.setTextColor(ContextCompat.getColor(this, R.color.inactiveText));
        locationTextView.setTextColor(ContextCompat.getColor(this, R.color.inactiveText));
        frameTextView.setTextColor(ContextCompat.getColor(this, R.color.inactiveText));
    }


    //=============================
    // Location Method
    //=============================

    private void addSelectLocationToImage(int itemLocationIndex) {
        mCurrentLocationFrameIndex = itemLocationIndex;
        View view = mViewpagerLocation.findViewWithTag(itemLocationIndex).findViewById(R.id.location_frame_layout);
        ((ViewGroup) view.getParent()).removeView(view);
        mCurrentLocationView = view;
        mCurrentLocationView = setLayoutParamOfLocationView(itemLocationIndex, mCurrentLocationView);
        filter_imageContainer_layout.addView(mCurrentLocationView);
        AnswersManager.logUserSelectDecorateLocationTemplate(itemLocationIndex);
    }

    private View setLayoutParamOfLocationView(int itemLocationIndex, View mCurrentLocationView) {
        Rect rect = getRectSizeOfImage();

        ViewGroup.LayoutParams oldParam = mCurrentLocationView.getLayoutParams();

        int bottomMargin = 0;
        int rightMargin = 0;

        if (oldParam instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams oldRelativeLayoutParam = (RelativeLayout.LayoutParams) oldParam;
            bottomMargin = oldRelativeLayoutParam.bottomMargin;
            rightMargin = oldRelativeLayoutParam.rightMargin;
        }

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(oldParam.width, oldParam.height);

        if (itemLocationIndex == 2) {
            layoutParams.gravity = Gravity.BOTTOM;
            layoutParams.setMargins(rect.left, 0, 0, rect.top);
            mCurrentLocationView.setLayoutParams(layoutParams);
            mCurrentLocationView.getLayoutParams().width = rect.width();
        } else if (itemLocationIndex == 1) {
            layoutParams.gravity = Gravity.BOTTOM | Gravity.RIGHT;
            layoutParams.setMargins(0, 0, rect.left, rect.top + bottomMargin);
            mCurrentLocationView.setLayoutParams(layoutParams);
        } else if (itemLocationIndex == 7) {
            layoutParams.gravity = Gravity.BOTTOM | Gravity.RIGHT;
            layoutParams.setMargins(0, 0, rect.left + rightMargin, rect.top);
            mCurrentLocationView.setLayoutParams(layoutParams);
        } else {
            layoutParams.gravity = Gravity.CENTER;
            mCurrentLocationView.setLayoutParams(layoutParams);
        }

        return mCurrentLocationView;
    }

    private void getCurrentLocation() {

        showLoading();

        SmartLocation.with(this).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        mCurrentLatLng = location.getLatitude() + "," + location.getLongitude();
                        getVenuesFromFoursquare(mCurrentLatLng);
                    }
                });

    }

    private void getVenuesFromFoursquare(String latlng) {
        FoursquareHelper.getInstance().getPlace(latlng, new FoursquareCallback() {
            @Override
            public void onSuccess(List<FoursquareVenues> venuesList) {

                hideLoading();

                if (venuesList.isEmpty()) {
                    return;
                }

                //Init Location Selector
                mLocationSelectorView = new LocationSelectorView(FilterActivity.this, venuesList, new LocationSelectorView.LocationSelectorViewInterface() {
                    @Override
                    public void didClickChooseLocation() {
                        if (mCurrentLatLng == null) {
                            return;
                        }
                        LocationSelectorActivity.startActivityForResult(FilterActivity.this, mCurrentLatLng);
                    }

                    @Override
                    public void didClickSelectLocation(FoursquareVenues venue) {
                        updateAllLocationViewWithVenues(venue);
                    }
                });

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, R.id.titleBar);
                mLocationSelectorView.setLayoutParams(params);
                rootView.addView(mLocationSelectorView);

                addLocationImageFromVenue(venuesList.get(0));
            }

            @Override
            public void onError() {
                hideLoading();
            }
        });
    }

    private void addLocationImageFromVenue(FoursquareVenues venue) {

        //remove existing one.
        if (mCurrentLocationView != null) {
            filter_imageContainer_layout.removeView(mCurrentLocationView);
        }

        mViewpagerLocation = new ViewPager(this);
        mViewpagerLocation.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        FramePagerAdapter mPagerAdapter = new FramePagerAdapter(venue, this);
        ;
        mViewpagerLocation.setAdapter(mPagerAdapter);

        CirclePageIndicator indicator = new CirclePageIndicator(this);
        FrameLayout.LayoutParams indicatorLayout = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        indicatorLayout.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        indicator.setPadding(20, 20, 20, 20);
        indicator.setLayoutParams(indicatorLayout);
        indicator.setViewPager(mViewpagerLocation);

        Rect imageRect = getRectSizeOfImage();

        FrameLayout mLocationSelectionLayout = new FrameLayout(this);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(imageRect.left, imageRect.top, 0, 0);
        mLocationSelectionLayout.setLayoutParams(params);
        mLocationSelectionLayout.getLayoutParams().width = imageRect.width();
        mLocationSelectionLayout.getLayoutParams().height = imageRect.height();

        mLocationSelectionLayout.addView(mViewpagerLocation);
        mLocationSelectionLayout.addView(indicator);

        filter_imageContainer_layout.addView(mLocationSelectionLayout);

        if (mCurrentLocationFrameIndex >= 0) {
            mViewpagerLocation.setCurrentItem(mCurrentLocationFrameIndex);
        }

    }

    //=============================
    // add sticker, text etc..
    //=============================
    private void addImageFromURI(Uri uri) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            Drawable drawable = Drawable.createFromStream(inputStream, uri.toString());
            boolean updateText = activeSticker != null;
            addImageFromDrawable(drawable, null, updateText);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addImageFromDrawable(Drawable pDrawable, EditingText pEditingText, boolean updateText) {

        NTSize size = ImageManager.ratioDrawableSize(pDrawable, Utils.convertDpToPixel(150, this));

        if (updateText) {
            activeSticker.setTag(pEditingText);
            activeSticker.setImageDrawable(pDrawable);
//            if (pEditingText.isKeepSize()) {
//                NTSize oldSize = ImageManager.ratioDrawableSize(pDrawable, activeSticker.getLayoutParams().width);
//                activeSticker.getLayoutParams().width = oldSize.getWidth();
//                activeSticker.getLayoutParams().height = oldSize.getHeight();
//            }
            return;
        }

        final StickerImageView sticker = new StickerImageView(this, size.getWidth(), size.getHeight());
        sticker.setImageDrawable(pDrawable);
        sticker.setmCallback(mStickerInterface);

        if (pEditingText != null) {
            sticker.setTag(pEditingText);
        }

        filter_imageContainer_layout.addView(sticker);

        StickerViewTouchHelper touchEngine = new StickerViewTouchHelper(this);
        touchEngine.setStickerTapCallback(new StickerViewTouchHelper.StickerTapCallback() {
            @Override
            public void onTap(StickerImageView imageView) {
                if (currentSubMenu == SubMenu.FONT || currentSubMenu == SubMenu.FONT_COLOR || currentSubMenu == SubMenu.FONT_OPACITY) {
                    return;
                }
                setNewActiveSticker(imageView, true);
            }

            @Override
            public void onDoubleTap() {
                if (sticker.getTag() != null && sticker.getTag() instanceof EditingText) {
                    setNewActiveSticker(sticker, true);
                    EditingText editingText = (EditingText) sticker.getTag();
                    EditTextActivity.openActivity(FilterActivity.this, editingText);
                }
            }
        });

        sticker.setOnTouchListener(touchEngine);
        setNewActiveSticker(sticker, pEditingText == null ? true : false);
    }

    private StickerView.StickerViewInterface mStickerInterface = new StickerView.StickerViewInterface() {
        @Override
        public void onClickClosed() {
            deleteActiveSticker();
        }
    };

    private void setNewActiveSticker(StickerImageView sticker, boolean visibleDeleteButton) {
        if (activeSticker == sticker) {
            return;
        }

        if (activeSticker != null) {
            activeSticker.setControlItemsHidden(true);
        }

        if (visibleDeleteButton) {
            int layoutId = currentTabMenu == null ? R.id.menuBar : R.id.progressViewLayoutForFragment;
            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            param.addRule(RelativeLayout.ABOVE, layoutId);
            okCancelStickerView.setLayoutParams(param);
            okCancelStickerView.setVisibility(View.VISIBLE);
        } else {
            removeOKCancelStickerView();
        }

        activeSticker = sticker;
        activeSticker.setControlItemsHidden(false);

        if (activeSticker.getTag() != null && currentTabMenu == TabMenu.TEXT) {
            enableTextFontAndColor(true);
        }
    }

    private void clearActiveSticker() {
        if (activeSticker != null) {
            activeSticker.setControlItemsHidden(true);
            if (activeSticker.getTag() != null && currentTabMenu == TabMenu.TEXT) {
                enableTextFontAndColor(false);
            }
        }
        removeOKCancelStickerView();
        activeSticker = null;
    }

    private void deleteActiveSticker() {
        if (activeSticker != null) {
            if (activeSticker.getParent() instanceof ViewGroup) {
                ((ViewGroup) activeSticker.getParent()).removeView(activeSticker);
                if (activeSticker.getTag() != null && currentTabMenu == TabMenu.TEXT) {
                    enableTextFontAndColor(false);
                }
                removeOKCancelStickerView();
                activeSticker = null;
            }
        }
    }

    //===========================================

    /**
     * Handle OnClick From Collection
     */
    //===========================================
    public void onClickFilter(FilterItem filterItem) {

        //if select old one.
        if (mCurrentFilterItem != null && mCurrentFilterItem == filterItem) {
            currentSubMenu = SubMenu.INTENSITY;
            double intensity = 50;
            if (mTempParam != null && mTempParam.getFilterImageName() == filterItem.getFilterLookupImageName()) {
                intensity = mTempParam.getIntensity() * 100;
            }
            ProgressMenuFragment fragment = ProgressMenuFragment.newInstance(new CollectionItem(filterItem.getItemName(), 0), SubMenu.INTENSITY, intensity);
            fragment.setMenuCallback(this);
            addFragment(R.id.progressViewLayoutForFragment, fragment, false);
            return;
        }

        AnswersManager.logUserSelectFilterImage(filterItem.getItemName());

        mCurrentFilterItem = filterItem;

        if (filterItem.getFilterLookupImageName() == R.drawable.original) {
            mFilterParam.setFilterImageName(0);
        } else {
            mFilterParam.setFilterImageName(mCurrentFilterItem.getFilterLookupImageName());
            mFilterParam.setIntensity(0.5f);
        }

        filterImage(mFilterParam);
    }

    public void onClickEffectCollection(CollectionItem item) {

        currentSubMenu = item.getSubMenu();

        if (currentSubMenu == SubMenu.CROP) {
            showCropMode();
            hideLocationAndFrameView();
            //re effect to original.
            filterImage(new FilterParam());
        } else if (currentSubMenu == SubMenu.ROTATE) {
            hideLocationAndFrameView();
        }

        double progressValue = mFilterParam.getProgressValueBySubMenu(currentSubMenu);

        ProgressMenuFragment fragment = ProgressMenuFragment.newInstance(item, currentSubMenu, progressValue);
        fragment.setMenuCallback(this);
        addFragment(R.id.progressViewLayoutForFragment, fragment, false);
    }

    private void hideLocationAndFrameView() {
        if (mCurrentLocationView != null) {
            mCurrentLocationView.animate().alpha(0).setDuration(ANIMATION_DURATION).start();
        }
        if (mCurrentFrame != null) {
            mCurrentFrame.animate().alpha(0).setDuration(ANIMATION_DURATION).start();
        }
    }

    private void showLocationAndFrameView(boolean isReload) {

        if (mCurrentLocationView != null) {
            if (isReload) {
                setLayoutParamOfLocationView(mCurrentLocationFrameIndex, mCurrentLocationView);
            }
            mCurrentLocationView.animate().alpha(1).setDuration(ANIMATION_DURATION).start();
        }
        if (mCurrentFrame != null) {
            if (isReload) {
                onClickFrame(mCurrentCollectionItemFrame);
            }
            mCurrentFrame.animate().alpha(1).setDuration(ANIMATION_DURATION).start();
        }
    }

    public void onClickAddSticker(StickerObject stickerObject) {
        Log.d("nut", stickerObject.getPath());
        Uri uri = Uri.fromFile(new File(stickerObject.getPath()));
        addImageFromURI(uri);
    }

    public void onClickShowFontList() {
        currentSubMenu = SubMenu.FONT;
        removeOKCancelStickerView();
        RecycleViewFragment fragment = RecycleViewFragment.newInstance(currentSubMenu);
        addFragment(R.id.progressViewLayoutForFragment, fragment, false);
    }

    public void onClickShowFontColorList() {
        currentSubMenu = SubMenu.FONT_COLOR;
        removeOKCancelStickerView();
        RecycleViewFragment fragment = RecycleViewFragment.newInstance(currentSubMenu);
        addFragment(R.id.progressViewLayoutForFragment, fragment, false);
    }

    public void onClickShowFontOpacity(CollectionItem item) {
        removeOKCancelStickerView();

        if (activeSticker == null || activeSticker.getTag() == null) {
            return;
        }

        currentSubMenu = SubMenu.FONT_OPACITY;
        EditingText existing = (EditingText) activeSticker.getTag();

        double progressValue = existing.getProgressValueFromOpacity();

        ProgressMenuFragment fragment = ProgressMenuFragment.newInstance(item, currentSubMenu, progressValue);
        fragment.setMenuCallback(this);
        addFragment(R.id.progressViewLayoutForFragment, fragment, false);
    }

    public void onClickAddTextFont(EditingText editingText) {

        boolean updateText = activeSticker != null && activeSticker.getTag() != null;

        //Add text font for first time
        if (!updateText) {
            onClickAddText(editingText);
            return;
        }

        //Update text font, text color
        EditingText existingEditingText = (EditingText) activeSticker.getTag();
        if (editingText.getTypefaceName() != null) {
            existingEditingText.setNewTypefaceName(editingText.getTypefaceName());
        }
        if (editingText.getResTextColorId() != 0) {
            existingEditingText.setNewResTextColorId(editingText.getResTextColorId());
        }

        existingEditingText.setKeepSize(true);

        onClickAddText(existingEditingText);
    }

    public void onClickAddText(EditingText editingText) {

        final float scale = getResources().getDisplayMetrics().density;
        int width = (int) (280 * scale);
        int padding = (int) (2 * scale);

        TextView tv = new TextView(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(layoutParams);
        tv.setText(editingText.getText());
        tv.setBackgroundColor(Color.TRANSPARENT);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 48);
        tv.setPadding(padding, padding, padding, padding);
        tv.setGravity(editingText.getGravity());

        if (editingText.getNewResTextColorId() != 0) {
            tv.setTextColor(editingText.getNewResTextColorId());
        } else if (editingText.getResTextColorId() != 0) {
            tv.setTextColor(editingText.getResTextColorId());
        } else {
            tv.setTextColor(Color.WHITE);
        }

        if (editingText.getNewTypefaceName() != null) {
            tv.setTypeface(Typeface.createFromAsset(getAssets(), editingText.getNewTypefaceName()));
        } else if (editingText.getTypefaceName() != null){
            tv.setTypeface(Typeface.createFromAsset(getAssets(), editingText.getTypefaceName()));
        }

        Paint paint = new Paint();
        Rect bounds = new Rect();
        paint.setTypeface(tv.getTypeface());// your preference here
        paint.setTextSize(tv.getTextSize());// have this the same as your text size
        String text = editingText.getText();
        paint.getTextBounds(text, 0, text.length(), bounds);
        int text_width =  bounds.width() + padding * 2;

        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        tv.measure(widthMeasureSpec, heightMeasureSpec);
        int height = tv.getMeasuredHeight();

        width = Math.min(text_width, width);

        Bitmap tempBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Log.d("nut", "widthMeasureSpec =" + width);
        Log.d("nut", "heightMeasureSpec =" + tv.getMeasuredHeight());

        Canvas c = new Canvas(tempBitmap);
        tv.layout(0, 0, tv.getMeasuredWidth(), tv.getMeasuredHeight());
        tv.draw(c);

        Drawable drawable = new BitmapDrawable(this.getResources(), tempBitmap);
        Log.d("nut", "drawable w =" + drawable.getIntrinsicWidth());
        Log.d("nut", "drawable h =" + drawable.getIntrinsicHeight());

        boolean updateText = activeSticker != null && activeSticker.getTag() != null;
        addImageFromDrawable(drawable, editingText, updateText);
    }

    private boolean checkLocationPermission() {

        if (!SmartLocation.with(this).location().state().locationServicesEnabled()) {
            DialogUtils.showMessageCancel(this, "Please turn on location service.", null);
            return false;
        }

        if (!SmartLocation.with(this).location().state().isNetworkAvailable() || !Utils.isOnline(this)) {
            DialogUtils.showMessageCancel(this, "Please connect to the internet.", null);
            return false;
        }

        int state = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (state == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else if (state != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                showNeedLocationPermissionDialog();

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS);
            }
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationAction();
                } else {
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);

                    if (showRationale) {
                        showNeedLocationPermissionDialog();
                    }
                }
                return;
            }
        }
    }

    private void showNeedLocationPermissionDialog() {
        DialogUtils.showAlertDialog(this, "Location Permission needed", "Access Location Permission is needed for get your location.", "Go to Setting", null, "OK", new DialogUtils.OnDialogClickedListener() {
            @Override
            public void onButtonClicked(int event, Bundle value) {

                if (event == POSITIVE) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 1080);
                }
            }
        });
    }


    public void onClickFrame(CollectionItem item) {

        if (mCurrentFrame != null) {
            filter_imageContainer_layout.removeView(mCurrentFrame);
        }

        if (item.getThumbnailImage() == R.drawable.frame_1) {
            mCurrentCollectionItemFrame = null;
            mCurrentFrame = null;
            return;
        }

        mCurrentFrame = new CustomFrameView(this, item);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);

        Rect rect = getRectSizeOfImage();

        params.setMargins(rect.left, rect.top, 0, 0);
        mCurrentFrame.setLayoutParams(params);
        mCurrentFrame.getLayoutParams().width = rect.width();
        mCurrentFrame.getLayoutParams().height = rect.height();
        filter_imageContainer_layout.addView(mCurrentFrame);

        mCurrentCollectionItemFrame = item;

        AnswersManager.logUserSelectDecorateFrame(item.getItemName().toLowerCase());
    }

    private void restoreStateDefault() {
        currentTabMenu = null;
        clearActiveSelectedMenu();
    }

    private void clearSubMenu() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.progressViewLayoutForFragment);

        if (fragment != null) {
            removeFragment(fragment);
        }

        if (currentTabMenu == TabMenu.TEXT) {
            currentTabMenu = null;
            textAction();
        }

        currentSubMenu = null;
    }

    private Rect getRectSizeOfImage() {
        RectF bitmapRect = new RectF();
        bitmapRect.right = imageView.getDrawable().getIntrinsicWidth();
        bitmapRect.bottom = imageView.getDrawable().getIntrinsicHeight();
        Matrix m = imageView.getImageMatrix();
        m.mapRect(bitmapRect);
        int width = (int) bitmapRect.width() + 1;
        int height = (int) bitmapRect.height() + 1;
        int top = (int) bitmapRect.top - 1;
        int left = (int) bitmapRect.left - 1;
        int right = (int) bitmapRect.right + 2;
        int bottom = (int) bitmapRect.bottom + 2;

        Rect r = new Rect(left, top, right, bottom);

        if (Math.abs(oldRotate) == 90 || Math.abs(oldRotate) == 270) {
            Rect newRect = new Rect(0, 0, r.height(), r.width());

            float scale = Math.min((float) imageView.getHeight() / newRect.height(), (float) imageView.getWidth() / newRect.width());

            newRect.right = (int) (newRect.right * scale);
            newRect.bottom = (int) (newRect.bottom * scale);
            newRect.left = (int) (imageView.getWidth() / 2f) - (int) (newRect.width() / 2f);
            newRect.top = (int) (imageView.getHeight() / 2f) - (int) (newRect.height() / 2f);
            newRect.bottom = newRect.top + newRect.bottom;
            newRect.right = newRect.left + newRect.right;

            return newRect;
        }

        return r;
    }

    public void setEffectValue(final double progressValue) {

        if (currentSubMenu == SubMenu.FONT_OPACITY) {
            if (activeSticker != null && activeSticker.getTag() != null) {
                EditingText existingEditingText = (EditingText) activeSticker.getTag();
                float opacity = existingEditingText.getOpacityFromProgressValue(progressValue);
                existingEditingText.setNewOpacity(opacity);
                activeSticker.getMainView().setAlpha(opacity);
            }
            return;
        }

        if (mCurrentFilterItem != null) {
            mTempParam.setFilterImageName(mCurrentFilterItem.getFilterLookupImageName());
        }

        mTempParam.setFilterValueBySubMenu(currentSubMenu, progressValue);

        filterImage(mTempParam);
    }


    /**
     * ProgressMenuFragment Action Callback
     */
    @Override
    public void onMenuClose() {

        if (currentTabMenu == TabMenu.FILTER) {
            if (currentSubMenu == SubMenu.INTENSITY) {
                filterImage(mFilterParam);
            }
        }
        if (currentTabMenu == TabMenu.EFFECT) {
            switch (currentSubMenu) {
                case BRIGHTNESS:
                case CONTRAST:
                case SATURATION:
                case SHARPEN:
                case FADE:
                    filterImage(mFilterParam);
                    break;
                case CROP:
                    if (currentSubMenu == SubMenu.CROP) {
                        hideCropMode();
                        imageView.startLoad(imageUri, new LoadCallback() {
                            @Override
                            public void onSuccess() {
                                rotateImage(oldRotate, 0);
                                filterImage(mFilterParam);
                            }

                            @Override
                            public void onError() {

                            }
                        });

                        showLocationAndFrameView(false);
                    }

                    break;
                case ROTATE:
                    //Reset Rotate
                    int rotateDiff = (newRotate - oldRotate) % 360;
                    newRotate = oldRotate;
                    reverseRotateImage(rotateDiff, ANIMATION_DURATION);
                    showLocationAndFrameView(false);
                    break;
            }
        } else if (currentTabMenu == TabMenu.LOCATION) {
            if (mViewpagerLocation != null) {
                filter_imageContainer_layout.removeView((ViewGroup) mViewpagerLocation.getParent());
                mViewpagerLocation = null;
                mCurrentLocationFrameIndex = 0;
            }
            restoreStateDefault();
            if (mLocationSelectorView != null) {
                rootView.removeView(mLocationSelectorView);
            }
        } else if (currentTabMenu == TabMenu.TEXT) {

            if (activeSticker != null && activeSticker.getTag() != null) {
                EditingText existingEditingtext = ((EditingText) activeSticker.getTag());
                if (existingEditingtext.isUpdate()) {
                    if (currentSubMenu == SubMenu.FONT_COLOR) {
                        existingEditingtext.setNewResTextColorId(0);
                    } else if (currentSubMenu == SubMenu.FONT) {
                        existingEditingtext.setNewTypefaceName(null);
                    } else if (currentSubMenu == SubMenu.FONT_OPACITY) {
                        EditingText existingEditingText = (EditingText) activeSticker.getTag();
                        activeSticker.setAlpha(existingEditingText.getOpacity());
                    }
                    onClickAddText(existingEditingtext);
                    clearActiveSticker();
                } else {
                    deleteActiveSticker();
                }
            }
        }

        clearSubMenu();
    }


    @Override
    public void onMenuOK() {

        if (currentTabMenu == TabMenu.FILTER) {
            if (currentSubMenu == SubMenu.INTENSITY) {
                mFilterParam.setIntensity(mTempParam.getIntensity());
            }
        } else if (currentTabMenu == TabMenu.EFFECT) {

            switch (currentSubMenu) {
                case BRIGHTNESS:
                    mFilterParam.setBrightness(mTempParam.getBrightness());
                    break;
                case CONTRAST:
                    mFilterParam.setContrast(mTempParam.getContrast());
                    break;
                case SATURATION:
                    mFilterParam.setSaturation(mTempParam.getSaturation());
                    break;
                case SHARPEN:
                    mFilterParam.setSharpen(mTempParam.getSharpen());
                    break;
                case FADE:
                    mFilterParam.setFade(mTempParam.getFade());
                    break;
                case ROTATE:
                    oldRotate = newRotate % 360;
                    mFilterParam.setIsRotate(oldRotate != 0);
                    showLocationAndFrameView(true);
                    break;
                case CROP:
                    mFilterParam.setIsCrop(true);
                    hideCropMode();
                    doCropImage();
                    break;
            }

            showRecyclerViewMenu(currentTabMenu);
        } else if (currentTabMenu == TabMenu.LOCATION) {
            if (mViewpagerLocation != null) {
                filter_imageContainer_layout.removeView((ViewGroup) mViewpagerLocation.getParent());
                addSelectLocationToImage(mViewpagerLocation.getCurrentItem());
                mViewpagerLocation = null;
            }
            restoreStateDefault();

            if (mLocationSelectorView != null) {
                rootView.removeView(mLocationSelectorView);
            }

        } else if (currentTabMenu == TabMenu.TEXT) {

            if (activeSticker != null && activeSticker.getTag() != null) {
                EditingText existingEditingtext = ((EditingText) activeSticker.getTag());
                if (existingEditingtext.isUpdate()) {
                    if (currentSubMenu == SubMenu.FONT_COLOR) {
                        existingEditingtext.setResTextColorId(existingEditingtext.getNewResTextColorId());
                        existingEditingtext.setNewResTextColorId(0);
                    } else if (currentSubMenu == SubMenu.FONT) {
                        existingEditingtext.setTypefaceName(existingEditingtext.getNewTypefaceName());
                        existingEditingtext.setNewTypefaceName(null);
                    } else if (currentSubMenu == SubMenu.FONT_OPACITY) {
                        EditingText existingEditingText = (EditingText) activeSticker.getTag();
                        existingEditingText.setOpacity((existingEditingText.getNewOpacity()));
                    }
                }

                existingEditingtext.setIsUpdate(true);
                onClickAddText(existingEditingtext);
            }

            clearActiveSticker();

        }

        clearSubMenu();
    }

    @Override
    public void onRotateLeft() {
        if (currentSubMenu == SubMenu.ROTATE) {
            newRotate -= 90;
            imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
        }
    }

    @Override
    public void onRotateRight() {
        if (currentSubMenu == SubMenu.ROTATE) {
            newRotate += 90;
            imageView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
        }
    }

    @OnClick({R.id.btnNext, R.id.btnBack, R.id.filter_layout, R.id.effect_layout, R.id.sticker_layout, R.id.text_layout, R.id.location_layout, R.id.frame_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.filter_layout:
                filterAction();
                break;
            case R.id.effect_layout:
                effectAction();
                break;
            case R.id.sticker_layout:
                stickerAction();
                break;
            case R.id.text_layout:
                textAction();
                break;
            case R.id.location_layout:
                locationAction();
                break;
            case R.id.frame_layout:
                frameAction();
                break;
            case R.id.btnBack:
                this.onBackPressed();
                break;
            case R.id.btnNext:
                openShare();
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == EditTextActivity.EDIT_TEXT_REQUEST_CODE) {
            EditingText editingText = data.getParcelableExtra("text");
            onClickAddText(editingText);
        } else if (requestCode == ShopActivity.GO_TO_SHOP_REQUEST_CODE) {
            //refresh sticker list.
            currentTabMenu = null;
            stickerAction();
        } else if (requestCode == LocationSelectorActivity.LOCATION_REQUEST_CODE) {
            FoursquareVenues venue = data.getParcelableExtra("selected_place");
            updateAllLocationViewWithVenues(venue);
        }
    }

    void updateAllLocationViewWithVenues(FoursquareVenues venue) {
        FramePagerAdapter adapter = (FramePagerAdapter) mViewpagerLocation.getAdapter();
        if (venue != null && adapter != null) {
            adapter.setVenue(venue);
        }
    }

    public StickerImageView getActiveSticker() {
        return activeSticker;
    }
}

