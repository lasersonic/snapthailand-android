package com.rgb72.adventureearth.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.EditingText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class EditTextActivity extends BaseActivity {

    public static final int EDIT_TEXT_REQUEST_CODE = 62782;
    private static final String ARGS_TEXT = "ARGS_TEXT";

    @Bind(R.id.editText_main)
    EditText mEditTextMain;

    private EditingText mEdittingText;

    public static void openActivity(Activity activity, EditingText editingText) {
        Intent intent = new Intent(activity, EditTextActivity.class);
        if (editingText != null) {
            intent.putExtra(ARGS_TEXT, editingText);
        }
        activity.startActivityForResult(intent, EditTextActivity.EDIT_TEXT_REQUEST_CODE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_edit_text);
        ButterKnife.bind(this);

        if (getIntent().getParcelableExtra(ARGS_TEXT) != null) {
            mEdittingText = getIntent().getParcelableExtra(ARGS_TEXT);
            setUpView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    public void setUpView() {
        mEditTextMain.setText(mEdittingText.getText());
        mEditTextMain.setGravity(mEdittingText.getGravity());
    }

    @OnClick({R.id.editText_close_button, R.id.editText_OK_button, R.id.editText_alignLeft_imageView, R.id.editText_alignCenter_imageView, R.id.editText_alignRight_imageView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.editText_close_button:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.editText_OK_button:
                mEdittingText.setText(mEditTextMain.getText().toString());
                Intent returnIntent = new Intent();
                returnIntent.putExtra("text", mEdittingText);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
                break;
            case R.id.editText_alignLeft_imageView:
                mEditTextMain.setGravity(Gravity.TOP|Gravity.LEFT);
                mEdittingText.setGravity(Gravity.TOP | Gravity.LEFT);
                break;
            case R.id.editText_alignCenter_imageView:
                mEditTextMain.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                mEdittingText.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                break;
            case R.id.editText_alignRight_imageView:
                mEditTextMain.setGravity(Gravity.TOP|Gravity.RIGHT);
                mEdittingText.setGravity(Gravity.TOP | Gravity.RIGHT);
                break;
        }
    }
}
