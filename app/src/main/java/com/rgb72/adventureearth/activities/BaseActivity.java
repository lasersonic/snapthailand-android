package com.rgb72.adventureearth.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import cc.cloudist.acplibrary.ACProgressPie;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by nutthawut on 10/23/16.
 */
public class BaseActivity extends FragmentActivity {


    private ACProgressFlower dialog;
    private ACProgressPie progress;

    public void showLoading() {
        if (dialog != null &&dialog.isShowing()) {
            return;
        }
        dialog = new ACProgressFlower.Builder(this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text("")
                .fadeColor(Color.DKGRAY).build();
        dialog.setCancelable(false);
        dialog.show();
    }

    public void showLoadingProgress() {

        progress = new ACProgressPie.Builder(this)
                .ringColor(Color.WHITE)
                .pieColor(Color.WHITE)
                .updateType(ACProgressConstant.PIE_MANUAL_UPDATE)
                .build();
        progress.setCancelable(false);
        progress.show();
    }


    public void setLoadingProgress(int p) {
        if(progress != null) {
            progress.setPiePercentage(p/100.0f);
        }
    }

    public void hideLoading() {
        if(dialog != null) {
            dialog.dismiss();
            dialog = null;
        }

        if(progress != null) {
            progress.dismiss();
            progress = null;
        }
    }

    void removeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .remove(fragment)
                .commit();
    }

    void replaceFragment(int containerId, Fragment fragment) {
        if(!isFinishing()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(containerId, fragment);
            ft.commit();
        }
    }

    void addFragment(int containerId, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(containerId, fragment);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        ft.commit();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
