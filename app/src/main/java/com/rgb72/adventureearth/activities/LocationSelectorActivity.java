package com.rgb72.adventureearth.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.SearchView;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.adapter.LocationAdapter;
import com.rgb72.adventureearth.api.FoursquareCallback;
import com.rgb72.adventureearth.api.FoursquareHelper;
import com.rgb72.adventureearth.model.foursquare.FoursquareVenues;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created by nutthawut on 5/13/17.
 */

public class LocationSelectorActivity extends BaseActivity {

    public static final String EXTRA_LAT_LNG =  "EXTRA_LAT_LNG";
    public static final int LOCATION_REQUEST_CODE =  2255;

    @Bind(R.id.navTitle)
    TextView navTitle;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.searchView)
    SearchView searchView;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private LocationAdapter locationAdapter;
    private Subscription subscriptionListView;
    private String currentLatLng = "";
    private List<FoursquareVenues> list;

    private final PublishSubject<String> onClickSubject = PublishSubject.create();

    public static void startActivityForResult(Activity activity, String argsLatLng) {
        Intent intent = new Intent(activity, LocationSelectorActivity.class);
        intent.putExtra(EXTRA_LAT_LNG, argsLatLng);
        activity.startActivityForResult(intent, LOCATION_REQUEST_CODE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_location_list);

        ButterKnife.bind(this);

        if ( getIntent() != null ) {
            currentLatLng = getIntent().getStringExtra(EXTRA_LAT_LNG);
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        list = new ArrayList<>();

        locationAdapter = new LocationAdapter(this, list);
        recyclerView.setAdapter(locationAdapter);

        subscriptionListView = locationAdapter.getPositionClicks().subscribe(new Action1<FoursquareVenues>() {
            @Override
            public void call(FoursquareVenues place) {
                Intent intent = new Intent();
                intent.putExtra("selected_place", place);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        onClickSubject.asObservable().debounce(650, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread()).subscribe(observer);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                onClickSubject.onNext(newText);
                return false;
            }
        });

        searchLocation("");

    }

    private Observer<String> observer = new Observer<String>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(String s) {
            searchLocation(s);
        }
    };

    private void searchLocation(String searchTerm) {

        FoursquareHelper.getInstance().getPlaceKeyword(currentLatLng, searchTerm, new FoursquareCallback() {
            @Override
            public void onSuccess(List<FoursquareVenues> venuesList) {
                list.clear();
                list.addAll(venuesList);
                locationAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError() {

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (subscriptionListView != null) {
            subscriptionListView.unsubscribe();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
