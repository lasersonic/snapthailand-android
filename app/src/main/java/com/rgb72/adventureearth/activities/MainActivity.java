package com.rgb72.adventureearth.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.desmond.squarecamera.CameraActivity;
import com.desmond.squarecamera.RuntimePermissionActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.adapter.BackgroundPagerAdapter;
import com.rgb72.adventureearth.api.API;
import com.rgb72.adventureearth.manager.AnswersManager;
import com.rgb72.adventureearth.utils.DialogUtils;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import rx.functions.Action1;

public class MainActivity extends BaseActivity {

    public static final int REQUEST_CAMERA = 0;
    private final int REQUEST_CAMERA_PERMISSION = 10;
    private final int REQUEST_GALLERY_PERMISSION = 11;
    private Uri mPhotoUri;

    @Bind(R.id.btn_camera)
    Button btnCamera;
    @Bind(R.id.btn_gallery)
    Button btnGallery;
    @Bind(R.id.linearLayout)
    LinearLayout linearLayout;
    @Bind(R.id.imageLogo)
    ImageView imageLogo;
    @Bind(R.id.btn_setting)
    ImageButton btnSetting;
    //    @Bind(R.id.view_pager)
    //    ViewPager viewPager;
    @Bind(R.id.tvImageName)
    TextView tvImageName;
    @Bind(R.id.viewFlipper)
    ViewFlipper viewFlipper;
    @Bind(R.id.rootView)
    RelativeLayout rootView;
    @Bind(R.id.facebookLoginView)
    RelativeLayout facebookLoginView;
    @Bind(R.id.facebookLoginButton)
    LoginButton fbLoginButton;

    private CallbackManager callbackManager;
    private Handler mTimerHandler = new Handler();
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics(), new Answers());

        setContentView(R.layout.fragment_home);

        ButterKnife.bind(this);

        startViewFliper();

        initFacebookLoginScreen();
    }

    private void startViewPager() {

        /***
         viewPager.setAdapter(new BackgroundPagerAdapter(this));

         CirclePageIndicator indicator = new CirclePageIndicator(this);
         RelativeLayout.LayoutParams indicatorLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
         indicatorLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
         indicatorLayout.addRule(RelativeLayout.CENTER_HORIZONTAL);
         indicator.setPadding(20, 20, 20, 20);
         indicator.setLayoutParams(indicatorLayout);
         indicator.setViewPager(viewPager);

         rootView.addView(indicator);
         **/
    }

    private void startViewFliper() {

        viewFlipper.setInAnimation(MainActivity.this, R.anim.view_transition_in_left);
        viewFlipper.setOutAnimation(MainActivity.this, R.anim.view_transition_out_left);

        runnable = new Runnable() {
            public void run() {
                viewFlipper.showNext();
                displayImageNameText();
                mTimerHandler.postDelayed(runnable, 3500);
            }
        };

        mTimerHandler.post(runnable);
        displayImageNameText();
    }

    private void displayImageNameText() {
        String text = "";
        switch (viewFlipper.getDisplayedChild()) {
            case 0:
                text = "Bangkok Street";
                break;
            case 1:
                text = "Chiang Mai Night";
                break;
            case 2:
                text = "Peak of Samui Island";
                break;
            case 3:
                text = "Phangan, waiting for full moon party";
                break;
            default:
                text = "Exotic Phi Phi Island";
                break;
        }

        tvImageName.setText(text);
    }

    private void initFacebookLoginScreen() {
        // if not login facebook yet.
        if (AccessToken.getCurrentAccessToken() == null) {

            callbackManager = CallbackManager.Factory.create();

            fbLoginButton.setReadPermissions("public_profile,email");
            fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    facebookLoginView.setVisibility(View.GONE);
                    getMe();
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {

                }
            });
        } else {
            facebookLoginView.setVisibility(View.GONE);
        }
    }

    private void getMe() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken == null) {
            return;
        }

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        API.getInstance(getApplicationContext()).sendLoginFacebookStat(object, null);
                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,birthday,email");
        request.setParameters(parameters);
        request.executeAsync();
    }


    public void startCameraActivity() {
        Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
    }

    @OnClick(R.id.btn_camera)
    void onCameraClick() {
        requestForCameraPermissionIfNeed();
    }

    @OnClick(R.id.btn_gallery)
    void onGalleryClick() {
        requestForGalleryPermission();
    }

    @OnClick(R.id.btn_setting)
    public void onClick() {
        goToSetting();
    }

    private void openGallery() {

        RxImagePicker.with(this).requestImage(Sources.GALLERY).subscribe(new Action1<Uri>() {
            @Override
            public void call(Uri uri) {
                AnswersManager.logImageSourceLibrary();
                FilterActivity.openActivity(MainActivity.this, uri);
            }
        });

    }

    public void goToSetting() {
        SettingActivity.openActivity(this);
    }

    /**
     * Life Cycle
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CAMERA) {
            mPhotoUri = data.getData();
            if (mPhotoUri == null) {
                mPhotoUri = data.getParcelableExtra("uri");
            }
        }

        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {

            boolean hasCameraPerm = false;
            boolean hasGalleryPerm = false;

            for (int i = 0; i < permissions.length; i++) {
                String perm = permissions[i];
                if (perm.equals(Manifest.permission.CAMERA) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    hasCameraPerm = true;
                }
                if (perm.equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    hasGalleryPerm = true;
                }
            }

            if (hasCameraPerm && hasGalleryPerm) {
                startCameraActivity();
            } else {

                DialogUtils.showMessageOKCancel(this, getString(R.string.camera_permission_denied), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                }, null);

            }
        } else if (requestCode == REQUEST_GALLERY_PERMISSION) {

            for (int i = 0; i < permissions.length; i++) {
                String perm = permissions[i];
                if (perm.equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                    return;
                }
            }

            DialogUtils.showMessageOKCancel(this, getString(R.string.camera_permission_denied), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            }, null);
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        if (mPhotoUri != null) {
            AnswersManager.logImageSourceCamera();
            FilterActivity.openActivity(MainActivity.this, mPhotoUri);
            mPhotoUri = null;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    // Check for camera permission in MashMallow
    public void requestForCameraPermissionIfNeed() {
        final String permissionCamera = Manifest.permission.CAMERA;
        final String permissionExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(MainActivity.this, permissionCamera)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionCamera)) {
                // Show permission rationale

                RuntimePermissionActivity.startActivity(MainActivity.this,
                        REQUEST_CAMERA_PERMISSION,
                        Manifest.permission.CAMERA);

            } else {
                // Handle the result in Activity#onRequestPermissionResult(int, String[], int[])
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permissionCamera, permissionExternalStorage}, REQUEST_CAMERA_PERMISSION);
            }
        } else if (ContextCompat.checkSelfPermission(MainActivity.this, permissionExternalStorage) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissionExternalStorage)) {
                // Show permission rationale
                RuntimePermissionActivity.startActivity(MainActivity.this,
                        REQUEST_CAMERA_PERMISSION,
                        permissionExternalStorage);

            } else {
                // Handle the result in Activity#onRequestPermissionResult(int, String[], int[])
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permissionCamera, permissionExternalStorage}, REQUEST_CAMERA_PERMISSION);
            }
        } else {
            // Start CameraActivity
            startCameraActivity();
        }
    }


    private void requestForGalleryPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show permission rationale
                RuntimePermissionActivity.startActivity(MainActivity.this,
                        REQUEST_GALLERY_PERMISSION,
                        Manifest.permission.READ_EXTERNAL_STORAGE);

            } else {
                // Handle the result in Activity#onRequestPermissionResult(int, String[], int[])
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_GALLERY_PERMISSION);
            }
        } else {
            // Start CameraActivity
            openGallery();
        }
    }


}
