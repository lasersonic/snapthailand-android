package com.rgb72.adventureearth.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.SkuDetails;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.adapter.ShopAdapter;
import com.rgb72.adventureearth.api.API;
import com.rgb72.adventureearth.api.DownloadProgressInterceptor;
import com.rgb72.adventureearth.manager.AnswersManager;
import com.rgb72.adventureearth.manager.InAppWrapper;
import com.rgb72.adventureearth.manager.RealmHelper;
import com.rgb72.adventureearth.model.ShopItem;
import com.rgb72.adventureearth.manager.UnzipManager;
import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.functions.Action1;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopActivity extends BaseActivity {

    public static final int GO_TO_SHOP_REQUEST_CODE = 3362;
    private InAppWrapper inAppWrapper;

    @Bind(R.id.navTitle)
    TextView navTitle;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private ArrayList<ShopItem> mShopItemList;
    private ArrayList<StickerCollection> mDownloadedStickerList;
    private ShopAdapter mShopAdapter;
    private Subscription subscriptionListViewButton, subscriptionListView;
    private long timestamp = 0;


    public static void openActivity(Context pContext) {
        pContext.startActivity(new Intent(pContext, ShopActivity.class));
    }

    public static void startActivityForResult(Activity pContext) {
        pContext.startActivityForResult(new Intent(pContext, ShopActivity.class), GO_TO_SHOP_REQUEST_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_shop);
        ButterKnife.bind(this);

        inAppWrapper = new InAppWrapper(this);
        mShopItemList = new ArrayList();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        this.loadStickerShop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupRecyclerView();
    }

    private void setupRecyclerView() {

        mDownloadedStickerList = RealmHelper.listStickerCollection(this);

        mShopAdapter = new ShopAdapter(this, mShopItemList, mDownloadedStickerList);
        recyclerView.setAdapter(mShopAdapter);

        subscriptionListView = mShopAdapter.getPositionClicks().subscribe(new Action1<ShopItem>() {
            @Override
            public void call(ShopItem shopItem) {
                ShopDetailActivity.openActivity(ShopActivity.this, shopItem);
            }
        });

        subscriptionListViewButton = mShopAdapter.getShopItemBuyObservable().subscribe(new Action1<ShopItem>() {
            @Override
            public void call(ShopItem shopItem) {
                //purchase(shopItem);

                if (shopItem.getPrice() == null) {
                    return;
                }

                if (shopItem.getPrice() > 0) {
                    purchase(shopItem);
                }
                else {
                    download(shopItem);
                }

            }
        });
    }

    private void loadStickerShop() {

        showLoading();

        API.getInstance(this).getStickerShop(new API.CallbackShop() {
            @Override
            public void onSuccess(List<ShopItem> items) {

                for (ShopItem item : items) {
                    SkuDetails skuDetails = getSKUFromItem(item);
                    if (inAppWrapper.getBp().isPurchased(item.getAndroid_inapp_id())) {
                        item.setPrice(0d);
                        item.setPrice_text(getString(R.string.buttonDownload));
                    }
                    else if (skuDetails != null) {
                        item.setPrice(skuDetails.priceValue);
                        item.setPrice_text(skuDetails.priceText);
                    } else {
                        item.setPrice(0d);
                        item.setPrice_text(getString(R.string.free));
                    }

                    mShopItemList.add(item);
                }

                mShopAdapter.notifyDataSetChanged();
                hideLoading();
            }

            @Override
            public void onFail(String reason) {
                hideLoading();
                final String mReason = reason;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ShopActivity.this, mReason, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private SkuDetails getSKUFromItem(ShopItem item) {
        return inAppWrapper.getBp().getPurchaseListingDetails(item.getAndroid_inapp_id());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!inAppWrapper.getBp().handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        ButterKnife.unbind(this);

        subscriptionListView.unsubscribe();
        subscriptionListViewButton.unsubscribe();

        if (inAppWrapper != null) {
            inAppWrapper.release();
        }

        super.onDestroy();
    }

    private void sendPurchased(final ShopItem item){
        API.getInstance(this).sendPurchased(item, new API.CallbackRequest() {
            @Override
            public void onSuccess(boolean success) {
                Log.d("nut", "sendPurchased = " + (success?"success":"fail"));
            }
        });
    }

    private void purchase(final ShopItem item) {

        boolean isAvailable = BillingProcessor.isIabServiceAvailable(this);

        if(!isAvailable) {
            Toast.makeText(getApplicationContext(), R.string.purchase_error, Toast.LENGTH_SHORT).show();
        }

        inAppWrapper.purchase(item, new InAppWrapper.InAppWrapperPurchaseCallback() {
            @Override
            public void onPurchaseSuccess() {
                AnswersManager.logUserBuySticker(item.getAndroid_inapp_id());
                sendPurchased(item);
                download(item);
            }

            @Override
            public void onPurchaseFail(Throwable error) {
                Toast.makeText(getApplicationContext(), R.string.purchase_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void download(final ShopItem item) {

        showLoadingProgress();

        API.getInstance(this).downloadSticker(item, new DownloadProgressInterceptor.DownloadProgressListener() {
            @Override
            public void update (long bytesRead, long contentLength, final boolean done) {
                if (!done) {
                    updateProgress(bytesRead, contentLength);
                }
            }

        }, new UnzipManager.UnzipManagerCallback() {
            @Override
            public void unzipSuccess() {
                setupRecyclerView();
                hideLoading();
            }

            @Override
            public void unzipFail(Exception e) {
                hideLoading();
            }
        });
    }

    private void updateProgress(final long bytesRead, final long contentLength) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                long currentTime = System.currentTimeMillis();

                if (currentTime - timestamp > 2000) {
                    timestamp = currentTime;
                    int progress = (int) (((float) bytesRead / (float) contentLength) * 100);
                    setLoadingProgress(progress);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK, new Intent());
        super.onBackPressed();
    }
}
