package com.rgb72.adventureearth.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.isseiaoki.simplecropview.CropImageView;

/**
 * Created by nutthawut on 11/17/16.
 */
public class NTImageView extends CropImageView {

    private Canvas temp;
    private Paint paint;
    private Paint p = new Paint();
    private Paint transparentPaint;

    public NTImageView(Context context) {
        super(context);
    }

    public NTImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NTImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

}

