package com.rgb72.adventureearth.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by nutthawut on 12/18/16.
 */
public class BubbleView extends View {

    private static final int DEFAULT_RANGE_COLOR = Color.argb(0xFF, 0x00, 0xCE, 0x5C);
    private static final int TRIANGLE_WIDTH = 8;
    private static final int TRIANGLE_HEIGHT = 8;
    private static final int SQUARE_WIDTH = 24;
    private static final int SQUARE_HEIGHT = 24;
    private static final int CORNER_RADIUS = 4;
    private static final int FONT_SIZE_PX = 12;
    private String progressValue = "0";

    public BubbleView(Context context) {
        super(context);
    }

    public BubbleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BubbleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(px2dp(SQUARE_WIDTH), px2dp(SQUARE_HEIGHT) + px2dp(TRIANGLE_HEIGHT));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(DEFAULT_RANGE_COLOR);
        canvas.drawRoundRect(new RectF(0, 0, getWidth(), px2dp(SQUARE_HEIGHT)), px2dp(CORNER_RADIUS), px2dp(CORNER_RADIUS), paint);
        drawTriangle(getWidth() / 2 - px2dp(TRIANGLE_WIDTH) / 2, px2dp(SQUARE_HEIGHT), px2dp(TRIANGLE_WIDTH), px2dp(TRIANGLE_HEIGHT), paint, canvas);

        Paint textPaint = new Paint(Paint.LINEAR_TEXT_FLAG);

        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(px2sp(FONT_SIZE_PX));
        textPaint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(progressValue, px2dp(SQUARE_WIDTH) / 2, px2dp(SQUARE_HEIGHT) / 2 - (textPaint.descent() + textPaint.ascent()) / 2, textPaint);
    }

    private void drawTriangle(int x, int y, int width, int height, Paint paint, Canvas canvas) {
        Point p1 = new Point(x, y);
        int pointX = x + width / 2;
        int pointY = y + height;
        Point p2 = new Point(pointX, pointY);
        Point p3 = new Point(x + width, y);
        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(p1.x, p1.y);
        path.lineTo(p2.x, p2.y);
        path.lineTo(p3.x, p3.y);
        path.close();
        canvas.drawPath(path, paint);
    }

    public void setProgressValue(String progressValue) {
        this.progressValue = progressValue;
    }

    private int px2dp(float px)  {
        return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, getResources().getDisplayMetrics());
    }
    private int px2sp(float px)  {
        return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, px, getResources().getDisplayMetrics());
    }

    public void setPositionX(float positionX) {
        setX(positionX - getWidth()/4);
    }
}
