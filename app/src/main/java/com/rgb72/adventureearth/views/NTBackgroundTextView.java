package com.rgb72.adventureearth.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.rgb72.adventureearth.R;

/**
 * Created by nutthawut on 12/4/16.
 */
public class NTBackgroundTextView extends TextView{

    public NTBackgroundTextView(Context context) {
        super(context);
        setBackgroundOnlyText("Test text");
    }

    public NTBackgroundTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundOnlyText("Test text");
    }

    public NTBackgroundTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setBackgroundOnlyText("Test text");
    }

    public void setBackgroundOnlyText(String myString) {
        Spannable spanna = new SpannableString(myString);
        spanna.setSpan(new BackgroundColorSpan(0xFF000000),0, myString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setText(spanna);
    }
}
