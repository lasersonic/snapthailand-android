package com.rgb72.adventureearth.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.isseiaoki.simplecropview.CropImageView;

/**
 * Created by nutthawut on 12/11/16.
 */
public class NTCropImageView extends CropImageView {

    public interface NTCropImageViewInterface{
        void onTouchEvent();
    }

    private NTCropImageViewInterface mCallback;

    public NTCropImageView(Context context) {
        super(context);
    }

    public NTCropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NTCropImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setTouchCallback(NTCropImageViewInterface callback) {
        mCallback = callback;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (mCallback != null) {
            mCallback.onTouchEvent();
        }
        return super.onTouchEvent(event);
    }

}
