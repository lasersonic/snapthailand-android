package com.rgb72.adventureearth.views;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.graphics.PointF;
import android.widget.ImageView;

import com.knef.stickerview.*;
import com.knef.stickerview.StickerView;

/**
 * Created by appsynth on 11/21/2016 AD.
 */

public class StickerViewTouchHelper implements View.OnTouchListener{

    public interface StickerTapCallback {
        void onTap(com.knef.stickerview.StickerImageView imageView);
        void onDoubleTap();
    }

    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;

    private float oldDist = 1f;
    private float d = 0f;

    private float dx; // postTranslate X distance
    private float dy; // postTranslate Y distance

    private float scale_orgWidth = 0;
    private float scale_orgHeight = 0;

    private StickerTapCallback stickerTapCallback;
    private GestureDetector gestureDetector;
    private Context mContext;
    private com.knef.stickerview.StickerImageView mImageView;

    public StickerViewTouchHelper(Context context) {
        mContext = context;
        gestureDetector = new GestureDetector(context, new DoubleTapListener());
    }

    public boolean onTouch(View v, MotionEvent event) {

        if (mImageView == null) {
            mImageView = (com.knef.stickerview.StickerImageView) v;
        }

        gestureDetector.onTouchEvent(event);

        final int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mode = DRAG;
                dx = mImageView.getX() - event.getRawX();
                dy = mImageView.getY() - event.getRawY();
            break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                if (oldDist > 10f) {
                    mode = ZOOM;
                    scale_orgWidth = mImageView.getLayoutParams().width;
                    scale_orgHeight = mImageView.getLayoutParams().height;
                }
                d = rotation(event);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                oldDist = 0;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    mImageView.setX(event.getRawX() + dx);
                    mImageView.setY(event.getRawY() + dy);

                } else if (mode == ZOOM) {
                    float newDist = spacing(event);
                    float newRot = rotation(event);
                    float rotation = newRot - d;
                    float scale = newDist / oldDist;


                    if ((scale >= 1.05 || scale <= 0.95) && !(scale > 3 || scale < -3)) {
                        float newWidth = scale * scale_orgWidth;
                        float newHeight = scale * scale_orgHeight;
                        mImageView.getLayoutParams().width = (int)newWidth;
                        mImageView.getLayoutParams().height = (int)newHeight;
                    }

                    mImageView.animate()
                        .rotationBy(rotation)
                        .setDuration(0)
                        .start();

                    mImageView.postInvalidate();
                    mImageView.requestLayout();
                }
                break;
        }

        return true;
    }
    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }

    public static float angleBetween2Lines(PointF A1, PointF A2, PointF B1, PointF B2) {
        float angle1 = (float) Math.atan2(A2.y - A1.y, A1.x - A2.x);
        float angle2 = (float) Math.atan2(B2.y - B1.y, B1.x - B2.x);
        float calculatedAngle = (float) Math.toDegrees(angle1 - angle2);
        if (calculatedAngle < 0) calculatedAngle += 360;
        return calculatedAngle;
    }


    public void setStickerTapCallback(StickerTapCallback stickerTapCallback) {
        this.stickerTapCallback = stickerTapCallback;
    }

    private class DoubleTapListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            if (stickerTapCallback != null) {
                stickerTapCallback.onTap(mImageView);
            }
            return true;
        }
        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            float x = e.getX();
            float y = e.getY();

            Log.d("nut", "Tapped at: (" + x + "," + y + ")");

            if (stickerTapCallback != null) {
                stickerTapCallback.onDoubleTap();
            }

            return true;
        }
    }
}
