package com.rgb72.adventureearth.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.View;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.CollectionItem;

/**
 * Created by nutthawut on 11/17/16.
 */
public class CustomFrameView extends View {

    private Bitmap bitmap;
    private CollectionItem mItem;

    public CustomFrameView(Context context) {
        super(context);
    }

    public CustomFrameView(Context context, CollectionItem pItem) {
        super(context);
        mItem = pItem;
    }

    public CustomFrameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomFrameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (bitmap == null) {
            createWindowFrame();
        }
        canvas.drawBitmap(bitmap, 0, 0, null);
    }

    protected void createWindowFrame() {

        final int CORNER_RADIUS = 20;

        Rect bound = new Rect(0, 0, getWidth(), getHeight());
        RectF outerRectangle = new RectF(bound.left, bound.top, bound.right, bound.bottom);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        Canvas osCanvas = null;

        if (mItem.getThumbnailImage() == R.drawable.frame_2) {
            bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            BitmapDrawable bm = new BitmapDrawable(getResources(), bitmap);
            bm.setBounds(bound);
            osCanvas = new Canvas(bitmap);
            paint.setColor(getResources().getColor(R.color.whiteColor));
            osCanvas.drawRect(outerRectangle, paint);
            outerRectangle.inset(22, 22);
        }
        else if (mItem.getThumbnailImage() == R.drawable.frame_3) {
            bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            BitmapDrawable bm = new BitmapDrawable(getResources(), bitmap);
            bm.setBounds(bound);
            osCanvas = new Canvas(bm.getBitmap());
            paint.setColor(getResources().getColor(R.color.black));
            osCanvas.drawRect(outerRectangle, paint);
            outerRectangle.inset(22, 22);
        }
        else if (mItem.getThumbnailImage() == R.drawable.frame_4) {
            BitmapDrawable bm = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getContext().getResources(),
                    R.drawable.postcard_texture));
            bm.setBounds(bound);
            bm.setTileModeY(Shader.TileMode.REPEAT);
            bm.setTileModeX(Shader.TileMode.REPEAT);
            bitmap = Bitmap.createBitmap(getWidth(), getHeight(), bm.getBitmap().getConfig());
            osCanvas = new Canvas(bitmap);
            bm.draw(osCanvas);
            outerRectangle.inset(22, 22);
        }
        else if (mItem.getThumbnailImage() == R.drawable.frame_5) {
            BitmapDrawable bottomImage = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getContext().getResources(),
                    R.drawable.movie_bottom));
            BitmapDrawable topImage = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getContext().getResources(),
                    R.drawable.movie_top));

            Rect topRect = new Rect(0, 0, getWidth(), topImage.getIntrinsicHeight());
            Rect bottomRect = new Rect(0, getHeight() - topImage.getIntrinsicHeight(), getWidth(), getHeight());
            topImage.setBounds(topRect);
            topImage.setTileModeX(Shader.TileMode.REPEAT);
            bottomImage.setTileModeX(Shader.TileMode.REPEAT);

            bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);

            osCanvas = new Canvas(bitmap);

            outerRectangle.inset(0, bottomImage.getIntrinsicHeight());

            topImage.draw(osCanvas);
            bottomImage.draw(osCanvas);

            for (int i=0;i<=getWidth();i+=bottomImage.getIntrinsicWidth()) {
                osCanvas.drawBitmap(bottomImage.getBitmap(), i, bottomRect.top, null);
            }

            paint.setColor(getResources().getColor(R.color.black));
            osCanvas.drawRect(outerRectangle, paint);
            outerRectangle.inset(22, 8);

        }
        else if (mItem.getThumbnailImage() == R.drawable.frame_6) {
            BitmapDrawable bm = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getContext().getResources(),
                    R.drawable.polaroid_texture));
            bm.setBounds(bound);
            bm.setTileModeY(Shader.TileMode.REPEAT);
            bm.setTileModeX(Shader.TileMode.REPEAT);
            bitmap = Bitmap.createBitmap(getWidth(), getHeight(), bm.getBitmap().getConfig());
            osCanvas = new Canvas(bitmap);
            bm.draw(osCanvas);
            outerRectangle.inset(80, 80);
            outerRectangle.bottom = (outerRectangle.height() - outerRectangle.top) * 0.85f;
        }

        paint.setColor(Color.TRANSPARENT);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        osCanvas.drawRoundRect(outerRectangle, CORNER_RADIUS, CORNER_RADIUS, paint);

    }

    @Override
    public boolean isInEditMode() {
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        bitmap = null;
    }


}
