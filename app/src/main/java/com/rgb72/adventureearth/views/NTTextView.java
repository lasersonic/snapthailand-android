package com.rgb72.adventureearth.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

import com.rgb72.adventureearth.R;

/**
 * Created by nutthawut on 12/4/16.
 */
public class NTTextView extends AppCompatTextView {


    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public NTTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context, attrs);
    }

    public NTTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.CustomFontTextView);

        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_font);
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);

        Typeface customFont = selectTypeface(context, fontName, textStyle);
        setTypeface(customFont);

        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, String fontName, int textStyle) {
        if (fontName.contentEquals(context.getString(R.string.font_name_kingbasillite))) {
            return Typeface.createFromAsset(context.getAssets(), "fonts/KingBasilLite.otf");
        }
        else if (fontName.contentEquals(context.getString(R.string.font_name_bromello))) {
            return Typeface.createFromAsset(context.getAssets(), "fonts/bromello.ttf");
        }
        else if (fontName.contentEquals(context.getString(R.string.font_name_frederick))) {
            return Typeface.createFromAsset(context.getAssets(), "fonts/FrederickatheGreat.ttf");
        }
        else if (fontName.contentEquals(context.getString(R.string.font_name_luna))) {
            return Typeface.createFromAsset(context.getAssets(), "fonts/Luna.ttf");
        }
        else if (fontName.contentEquals(context.getString(R.string.font_name_iannBKK))) {
            return Typeface.createFromAsset(context.getAssets(), "fonts/2006_iannnnnBKK.ttf");
        }
        else {
            // no matching font found
            // return null so Android just uses the standard font (Roboto)
            return null;
        }
    }
}
