package com.rgb72.adventureearth.views;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.activities.LocationSelectorActivity;
import com.rgb72.adventureearth.model.foursquare.FoursquareVenues;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nutthawut on 5/16/17.
 */

public class LocationSelectorView extends LinearLayout implements View.OnClickListener{

    public interface LocationSelectorViewInterface {
        void didClickChooseLocation();
        void didClickSelectLocation(FoursquareVenues venues);
    }

    @Bind(R.id.chooseLocationRelativeLayout)
    RelativeLayout chooseLocationRelativeLayout;
    @Bind(R.id.textViewLocation1)
    TextView textViewLocation1;
    @Bind(R.id.textViewLocation2)
    TextView textViewLocation2;
    @Bind(R.id.textViewLocation3)
    TextView textViewLocation3;
    @Bind(R.id.textViewLocation4)
    TextView textViewLocation4;
    @Bind(R.id.textViewLocation5)
    TextView textViewLocation5;

    private LocationSelectorViewInterface callback;
    private Context context;
    private List<FoursquareVenues> mVenuesList;

    public LocationSelectorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initializeViews(context);
    }

    public LocationSelectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public LocationSelectorView(Context context) {
        super(context);
        this.context = context;
        initializeViews(context);
    }

    public LocationSelectorView(Context context, List<FoursquareVenues> venuesList, LocationSelectorViewInterface callback) {
        super(context);
        this.context = context;
        this.callback = callback;
        this.mVenuesList = venuesList;

        initializeViews(context);

        if (venuesList.size() < 5) {
            return;
        }

        textViewLocation1.setOnClickListener(this);
        textViewLocation2.setOnClickListener(this);
        textViewLocation3.setOnClickListener(this);
        textViewLocation4.setOnClickListener(this);
        textViewLocation5.setOnClickListener(this);

        for (int i = 0; i < 5; i++) {

            if (venuesList.size() < i) {
                return;
            }

            FoursquareVenues venues = venuesList.get(i);

            switch (i) {
                case 0:
                    textViewLocation1.setText(venues.getName());
                    break;
                case 1:
                    textViewLocation2.setText(venues.getName());
                    break;
                case 2:
                    textViewLocation3.setText(venues.getName());
                    break;
                case 3:
                    textViewLocation4.setText(venues.getName());
                    break;
                case 4:
                    textViewLocation5.setText(venues.getName());
                    break;
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initializeViews(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_location_selector, this);

        ButterKnife.bind(this, view);

        chooseLocationRelativeLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.didClickChooseLocation();
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v == textViewLocation1 ) {
            callback.didClickSelectLocation(mVenuesList.get(0));
        } else if (v == textViewLocation2 ) {
            callback.didClickSelectLocation(mVenuesList.get(1));
        } else if (v == textViewLocation3 ) {
            callback.didClickSelectLocation(mVenuesList.get(2));
        } else if (v == textViewLocation4 ) {
            callback.didClickSelectLocation(mVenuesList.get(3));
        } else if (v == textViewLocation5 ) {
            callback.didClickSelectLocation(mVenuesList.get(4));
        }
    }
}
