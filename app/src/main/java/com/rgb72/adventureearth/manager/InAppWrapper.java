package com.rgb72.adventureearth.manager;

import android.app.Activity;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.ShopItem;

/**
 * Created by nutthawut on 10/19/16.
 */
public class InAppWrapper implements BillingProcessor.IBillingHandler {

    public interface InAppWrapperPurchaseCallback {
        void onPurchaseSuccess();
        void onPurchaseFail(Throwable error);
    }

    public interface InAppWrapperRestoreCallback {
        void onRestoreSuccess();
    }

    private Activity mActivity;
    private BillingProcessor bp;
    private InAppWrapperPurchaseCallback mPurchaseCallback;
    private InAppWrapperRestoreCallback mRestoreCallback;

    public InAppWrapper(Activity act) {
        mActivity = act;
        bp = new BillingProcessor(act, mActivity.getString(R.string.inappKey), this);
    }

    public void purchase(ShopItem pShopItem, InAppWrapperPurchaseCallback pCallback) {
        if (bp == null) {
            return;
        }
        mPurchaseCallback = pCallback;
        bp.purchase(mActivity, pShopItem.getAndroid_inapp_id());
    }

    public boolean restore(InAppWrapperRestoreCallback pCallback) {
        if (bp == null) {
            return false;
        }
        mRestoreCallback = pCallback;
        boolean success = bp.loadOwnedPurchasesFromGoogle();
        return success;
    }


    public void release() {
        if (bp != null) {
            bp.release();
        }
    }

    /*
     *  IBillingHandler implementation
     */
    @Override
    public void onBillingInitialized() {
        /*
         * Called when BillingProcessor was initialized and it's ready to purchase
         */

//        SkuDetails sku = bp.getPurchaseListingDetails("com.nuttang.snapthailand.sticker1");
//
//        if (sku == null) {
//            AlertDialog alert = new AlertDialog.Builder(mActivity).setTitle("SKU Is").setMessage("NULL SAD").create();
//            alert.show();
//        }
//        else {
//            AlertDialog alert = new AlertDialog.Builder(mActivity).setTitle("SKU Is").setMessage("ProductId -> " + sku.priceText).create();
//            alert.show();
//        }

    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        if (mPurchaseCallback != null) {
            mPurchaseCallback.onPurchaseSuccess();
        }
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        /*
         * Called when some error occurred. See Constants class for more details
         */
        if (mPurchaseCallback != null) {
            mPurchaseCallback.onPurchaseFail(error);
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {
        /*
         * Called when purchase history was restored and the list of all owned PRODUCT ID's
         * was loaded from Google Play
         */
        if (mRestoreCallback!=null) {
            mRestoreCallback.onRestoreSuccess();
        }
    }


    public BillingProcessor getBp() {
        return bp;
    }

}
