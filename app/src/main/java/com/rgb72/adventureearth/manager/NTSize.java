package com.rgb72.adventureearth.manager;

public class NTSize {
    private int width;
    private int height;

    NTSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
