package com.rgb72.adventureearth.manager;

import android.content.Context;

import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.model.realm.StickerCollectionRealm;
import com.rgb72.adventureearth.model.realm.StickerObjectRealm;

import java.util.ArrayList;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmMigration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.RealmSchema;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nutthawut on 11/10/16.
 */
public class RealmHelper {

    static final int DB_REALM_VERSION = 5;

    public static void saveSticker(Context context, final StickerCollection stickerCollection) {

        Realm.init(context);

        Realm realm = null;

        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(DB_REALM_VERSION) // Must be bumped when the schema changes
                .migration(migration) // Migration to run
                .build();

        Realm.setDefaultConfiguration(config);

        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealm(stickerCollection.toRealm());
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static ArrayList<StickerCollection> listStickerCollection(Context context) {

        ArrayList<StickerCollection> collections = new ArrayList<>();

        Realm.init(context);

        Realm realm = null;

        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(DB_REALM_VERSION) // Must be bumped when the schema changes
                .migration(migration) // Migration to run
                .build();
        Realm.setDefaultConfiguration(config);

        try {
            realm = Realm.getDefaultInstance();
            final RealmResults<StickerCollectionRealm> stickerCollectionRealms = realm.where(StickerCollectionRealm.class)
                    .findAllSorted("stickerOrderSequence", Sort.ASCENDING);

            for (StickerCollectionRealm managedCollection : stickerCollectionRealms) {
                collections.add(managedCollection.toModel());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (realm != null) {
                realm.close();
            }
        }
        return collections;
    }


    // Example migration adding a new class
    static RealmMigration migration = new RealmMigration() {
        @Override
        public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

            // DynamicRealm exposes an editable schema
            RealmSchema schema = realm.getSchema();

            if (oldVersion < 4) {
                schema.get("StickerCollectionRealm")
                        .addField("stickerOrderSequence", String.class, FieldAttribute.REQUIRED)
                        .setNullable("stickerOrderSequence", true);
            }

            if (oldVersion == 4) {
                schema.get("StickerCollectionRealm")
                        .addField("stickerThumbnailIndex", int.class);
            }

        }
    };

}
