package com.rgb72.adventureearth.manager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Size;

import com.desmond.squarecamera.ImageUtility;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by nutthawut on 11/9/16.
 */
public class ImageManager {

    public static Bitmap rotateImage(Context context, int rotation, byte[] data, int coverHeight) {

        Bitmap bitmapFromCamera = ImageUtility.decodeSampledBitmapFromByte(context, data);
        Bitmap bitmap = cropBitmap(bitmapFromCamera, coverHeight);

        //Bitmap bitmap = ImageUtility.decodeSampledBitmapFromByte(getActivity(), data);

        if (rotation != 0) {
            Bitmap oldBitmap = bitmap;

            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);

            bitmap = Bitmap.createBitmap(
                    oldBitmap, 0, 0, oldBitmap.getWidth(), oldBitmap.getHeight(), matrix, false
            );

            oldBitmap.recycle();
        }

        return bitmap;
    }

    public static Bitmap cropBitmap(Bitmap srcBitmap, int coverHeight) {
        int mCoverHeight = (int) ((float) coverHeight / 1.75f);
        Bitmap bmOverlay = Bitmap.createBitmap(srcBitmap, mCoverHeight, 0, srcBitmap.getWidth() - 2 * mCoverHeight, srcBitmap.getHeight());
        return bmOverlay;
    }

    public static Bitmap decodeSampledBitmapFromResource(Context context, Uri uri,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bm;
        FileDescriptor fileDescriptor = null;

        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            options.inJustDecodeBounds = true;

            ParcelFileDescriptor parcelFileDescriptor =
                    context.getContentResolver().openFileDescriptor(uri, "r");
            fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (fileDescriptor == null) {
            return null;
        }

        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

        // Calculate inSampleSize
        int size = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inSampleSize = size;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        return bm;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static NTSize ratioDrawableSize(Drawable pDrawable, int newSize) {

        int width = pDrawable.getIntrinsicWidth();
        int height = pDrawable.getIntrinsicHeight();
        float oldRatio = (float) width / height;

        if (width >= height) {
            width = newSize;
            height = (int) (width / oldRatio);
        } else {
            height = newSize;
            width = (int) (height * oldRatio);
        }

        return new NTSize(width, height);
    }
}

