package com.rgb72.adventureearth.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.rgb72.adventureearth.R;
import com.rgb72.adventureearth.model.FilterItem;
import com.rgb72.adventureearth.model.filter.FilterParam;

import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageLookupFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageOpacityFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSharpenFilter;

/**
 * Created by nutthawut on 9/17/16.
 */
public class FilterManager {

    private static FilterManager manager;

    public static FilterManager getInstance() {
        if(manager == null) {
            manager =  new FilterManager();
        }
        return manager;
    }

    public interface FilterManagerCallback {
        void filteredImage(Bitmap filterBitmap);
    }


    public static ArrayList<FilterItem> getFilterItem() {

        ArrayList<FilterItem> filterItems = new ArrayList<FilterItem>();

        filterItems.add(new FilterItem("Original", 0, R.drawable.original));
        filterItems.add(new FilterItem("Bangkok Street", 0, R.drawable.nut));
        filterItems.add(new FilterItem("Pattaya Sun", 0, R.drawable.etikate));
        filterItems.add(new FilterItem("Chiangmai vibe", 0, R.drawable.amatorka));
        filterItems.add(new FilterItem("Vintage Ayutthaya", 0, R.drawable.night));
        filterItems.add(new FilterItem("Phuket Sand", 0, R.drawable.breakfast));
        filterItems.add(new FilterItem("Lanta dive", 0, R.drawable.sepia));
        filterItems.add(new FilterItem("Fullmoon\nPha-ngan", 0, R.drawable.soft));
        filterItems.add(new FilterItem("Floating market", 0, R.drawable.soft2));
        filterItems.add(new FilterItem("PP Island", 0, R.drawable.lookup1));
        filterItems.add(new FilterItem("Sukhothai Rice", 0, R.drawable.nut2));


        return filterItems;

    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }


    public void appliedFilter(Context context, FilterItem filterItem, Bitmap originalImage, FilterManagerCallback callback) {
        GPUImage mGPUImage = new GPUImage(context);

        GPUImageLookupFilter lookupFilter = new GPUImageLookupFilter();
        lookupFilter.setBitmap(BitmapFactory.decodeResource(context.getResources(), filterItem.getFilterLookupImageName()));

        mGPUImage.setImage(originalImage);
        mGPUImage.setFilter(lookupFilter);

        Bitmap filteredImage = mGPUImage.getBitmapWithFilterApplied();
        callback.filteredImage(filteredImage);
    }
//
//    public void appliedFilter(Context context, FilterItem filterItem, Bitmap originalImage, float intensity, FilterManagerCallback callback) {
//        GPUImage mGPUImage = new GPUImage(context);
//
//
//        mGPUImage.setImage(originalImage);
//        mGPUImage.setFilter(lookupFilter);
//
//        Bitmap filteredImage = mGPUImage.getBitmapWithFilterApplied();
//        callback.filteredImage(filteredImage);
//    }

    public void doFilter(Context context, Bitmap originalImage, FilterParam param, FilterManagerCallback callback) {

        GPUImage mGPUImage = new GPUImage(context);

        GPUImageFilterGroup groupFilter = new GPUImageFilterGroup();

        if (param.getBrightness() != FilterParam.BRIGHTNESS_DEFAULT) {
            GPUImageBrightnessFilter brightnessFilter = new GPUImageBrightnessFilter();
            brightnessFilter.setBrightness(param.getBrightness());
            groupFilter.addFilter(brightnessFilter);
        }
        if (param.getContrast() != FilterParam.CONTRAST_DEFAULT) {
            GPUImageContrastFilter contrastFilter = new GPUImageContrastFilter();
            contrastFilter.setContrast(param.getContrast());
            groupFilter.addFilter(contrastFilter);
        }
        if (param.getSaturation() != FilterParam.SATURATION_DEFAULT) {
            GPUImageSaturationFilter saturationFilter = new GPUImageSaturationFilter();
            saturationFilter.setSaturation(param.getSaturation());
            groupFilter.addFilter(saturationFilter);
        }
        if (param.getSharpen() != FilterParam.SHARPEN_DEFAULT) {
            GPUImageSharpenFilter sharpenFilter = new GPUImageSharpenFilter();
            sharpenFilter.setSharpness(param.getSharpen());
            groupFilter.addFilter(sharpenFilter);
        }

        if (param.getFilterImageName() != 0) {
            GPUImageLookupFilter lookupFilter = new GPUImageLookupFilter();
            lookupFilter.setIntensity(param.getIntensity());
            lookupFilter.setBitmap(BitmapFactory.decodeResource(context.getResources(), param.getFilterImageName()));
            groupFilter.addFilter(lookupFilter);
        }


        if (groupFilter.getFilters().size() == 0) {
            callback.filteredImage(originalImage);
            return;
        }


        mGPUImage.setImage(originalImage);
        mGPUImage.setFilter(groupFilter);

        Bitmap filteredImage = mGPUImage.getBitmapWithFilterApplied();
        callback.filteredImage(filteredImage);
    }


}
