package com.rgb72.adventureearth.manager;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

/**
 * Created by nutthawut on 1/2/17.
 */
public class AnswersManager {

    public static void logImageSourceCamera() {
        log("User's image source", "source", "camera");
    }

    public static void logImageSourceLibrary() {
        log("User's image source", "source", "library");
    }

    public static void logUserSelectDecorateSticker(String stickerCollectionName) {
        log("User's select sticker", "collection_name", stickerCollectionName);
    }

    public static void logUserSelectDecorateLocationTemplate(int templateIndex) {
        log("User's select location template", "template", templateIndex);
    }

    public static void logUserSelectDecorateFrame(String frameName) {
        log("User's select frame", "frame_name", frameName);
    }

    public static void logUserBuySticker(String inAppId) {
        log("User's buy sticker", "product_id", inAppId);
    }

    public static void logUserSelectFilterImage(String filterName) {
        log("User's select filter", "filter_name", filterName);
    }

    public static void logUserSelectShareSocial(String method) {
        log("User's share image to", "method", method);
    }

    private static void log(String eventName, String key, String value) {
        Answers.getInstance().logCustom(new CustomEvent(eventName)
                .putCustomAttribute(key, value));
    }

    private static void log(String eventName, String key, int value) {
        Answers.getInstance().logCustom(new CustomEvent(eventName)
                .putCustomAttribute(key, value));
    }
}
