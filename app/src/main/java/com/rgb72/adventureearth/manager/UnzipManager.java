package com.rgb72.adventureearth.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.rgb72.adventureearth.manager.RealmHelper;
import com.rgb72.adventureearth.model.ShopItem;
import com.rgb72.adventureearth.model.realm.StickerCollection;
import com.rgb72.adventureearth.model.realm.StickerObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by nutthawut on 10/23/16.
 */
public class UnzipManager extends AsyncTask<String, Void, StickerCollection> {

    public interface UnzipManagerCallback {
        void unzipSuccess();

        void unzipFail(Exception e);
    }

    private Context mContext;
    private UnzipManagerCallback mCallback;
    private ShopItem mShopItem;

    public UnzipManager(Context context, ShopItem pItem, UnzipManagerCallback pCallback) {
        mContext = context;
        mShopItem = pItem;
        mCallback = pCallback;
    }

    private void handleDirectory(String destination, String dir) {
        File f = new File(destination + dir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }

    @Override
    protected StickerCollection doInBackground(String... args) {
        String filePath = args[0];
        String destination = args[1];

        File f = new File(destination);
        if (!f.exists()) {
            if (!f.mkdirs()) {
                return null;
            }
        }

        try {
            FileInputStream inputStream = new FileInputStream(filePath);
            ZipInputStream zipStream = new ZipInputStream(inputStream);
            ZipEntry zEntry;

            ArrayList<StickerObject> stickerObjects = new ArrayList<>();

            while ((zEntry = zipStream.getNextEntry()) != null) {
                Log.d("nut", "Unzipping " + zEntry.getName() + " at "
                        + destination);

                if (!(zEntry.getName().endsWith(".png")
                        || zEntry.getName().endsWith(".jpg")
                        || zEntry.getName().endsWith(".jpeg"))
                        || zEntry.getName().startsWith("__MACOSX")) {
                    Log.d("nut", "Skipping  " + zEntry.getName());
                    continue;
                }

                String file_path = destination + "/" + zEntry.getName();

                if (zEntry.isDirectory()) {
                    handleDirectory(destination, zEntry.getName());
                } else {
                    FileOutputStream fout = new FileOutputStream(file_path);
                    BufferedOutputStream bufout = new BufferedOutputStream(fout);
                    byte[] buffer = new byte[1024];
                    int read = 0;
                    while ((read = zipStream.read(buffer)) != -1) {
                        bufout.write(buffer, 0, read);
                    }

                    zipStream.closeEntry();
                    bufout.close();
                    fout.close();
                }

                StickerObject obj = new StickerObject();
                obj.setPath(file_path);
                stickerObjects.add(obj);
            }

            zipStream.close();

            Log.d("nut", "Unzipping complete. path : " + destination);

            if (mShopItem != null) {
                StickerCollection collection = new StickerCollection();
                collection.setStickerId(mShopItem.getItemID());
                collection.setStickerInAppId(mShopItem.getAndroid_inapp_id());
                collection.setStickerName(mShopItem.getItemName());
                collection.setStickerOrderSequence(mShopItem.getSequence_order());
                collection.setStickerLists(stickerObjects);
                collection.setStickerThumbnailIndex(mShopItem.getThumbnails().indexOf(mShopItem.getAvatar()));
                return collection;
            }


        } catch (Exception e) {
            Log.d("nut", "Unzipping failed");
            mCallback.unzipFail(e);
            e.printStackTrace();
            return null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(final StickerCollection collection) {

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (collection != null && mContext != null) {
                    RealmHelper.saveSticker(mContext, collection);
                    mCallback.unzipSuccess();
                }
            }
        });

        super.onPostExecute(collection);
    }

}
